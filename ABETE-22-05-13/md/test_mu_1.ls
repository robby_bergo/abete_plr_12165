/PROG  TEST_MU_1
/ATTR
OWNER		= MNEDITOR;
COMMENT		= "";
PROG_SIZE	= 1383;
CREATE		= DATE 13-04-09  TIME 03:31:26;
MODIFIED	= DATE 13-04-10  TIME 01:48:30;
FILE_NAME	= TEST_MU;
VERSION		= 0;
LINE_COUNT	= 29;
MEMORY_SIZE	= 1639;
PROTECT		= READ_WRITE;
TCD:  STACK_SIZE	= 0,
      TASK_PRIORITY	= 50,
      TIME_SLICE	= 0,
      BUSY_LAMP_OFF	= 0,
      ABORT_REQUEST	= 0,
      PAUSE_REQUEST	= 0;
DEFAULT_GROUP	= 1,*,*,*,*;
CONTROL_CODE	= 00000000 00000000;
/APPL

AUTO_SINGULARITY_HEADER;
  ENABLE_SINGULARITY_AVOIDANCE   : FALSE;
/MN
   1:  UFRAME_NUM=2 ;
   2:  UTOOL_NUM=1 ;
   3:   ;
   4:   ;
   5:J P[1] 30% FINE    ;
   6:   ;
   7:J P[2] 20% FINE    ;
   8:J P[3] 20% FINE    ;
   9:   ;
  10:J P[7] 20% FINE    ;
  11:   ;
  12:L P[4] 200mm/sec FINE    ;
  13:   ;
  14:L P[6] 100mm/sec FINE    ;
  15:   ;
  16:L P[5] 100mm/sec FINE    ;
  17:   ;
  18:L P[8] 200mm/sec FINE    ;
  19:   ;
  20:L P[9] 100mm/sec FINE    ;
  21:   ;
  22:   ;
  23:L P[11] 100mm/sec FINE    ;
  24:L P[10] 100mm/sec FINE    ;
  25:   ;
  26:L P[13] 100mm/sec FINE    ;
  27:L P[12] 100mm/sec FINE    ;
  28:   ;
  29:L P[14] 100mm/sec FINE    ;
/POS
P[1]{
   GP1:
	UF : 2, UT : 1,		CONFIG : 'N U T, 0, 0, 0',
	X = -1543.022  mm,	Y =   -45.619  mm,	Z =   144.995  mm,
	W =      .008 deg,	P =      .002 deg,	R =   179.999 deg
};
P[2]{
   GP1:
	UF : 2, UT : 1,		CONFIG : 'N U B, 0, 0, 0',
	X = -1318.526  mm,	Y =   -37.651  mm,	Z =   359.797  mm,
	W =     4.112 deg,	P =   -37.282 deg,	R =   172.085 deg
};
P[3]{
   GP1:
	UF : 2, UT : 1,		CONFIG : 'N U B, 0, 0, 0',
	X =  -951.611  mm,	Y =   -24.825  mm,	Z =   408.586  mm,
	W =     6.209 deg,	P =   -47.008 deg,	R =    -1.653 deg
};
P[4]{
   GP1:
	UF : 2, UT : 1,		CONFIG : 'N U T, 0, 0, 0',
	X =  -529.008  mm,	Y =     -.000  mm,	Z =   165.888  mm,
	W =     -.479 deg,	P =   -38.763 deg,	R =      .188 deg
};
P[5]{
   GP1:
	UF : 2, UT : 1,		CONFIG : 'N U T, 0, 0, 0',
	X =     0.000  mm,	Y =     -.000  mm,	Z =      .000  mm,
	W =     -.479 deg,	P =   -38.763 deg,	R =      .188 deg
};
P[6]{
   GP1:
	UF : 2, UT : 1,		CONFIG : 'N U T, 0, 0, 0',
	X =   -10.368  mm,	Y =     -.000  mm,	Z =    32.832  mm,
	W =     -.479 deg,	P =   -38.763 deg,	R =      .188 deg
};
P[7]{
   GP1:
	UF : 2, UT : 1,		CONFIG : 'N U T, 0, 0, 0',
	X =  -749.012  mm,	Y =     -.000  mm,	Z =   293.904  mm,
	W =     -.479 deg,	P =   -38.762 deg,	R =      .188 deg
};
P[8]{
   GP1:
	UF : 2, UT : 1,		CONFIG : 'N U T, 0, 0, 0',
	X =   -10.368  mm,	Y =     -.000  mm,	Z =    32.000  mm,
	W =     -.479 deg,	P =   -49.000 deg,	R =      .188 deg
};
P[9]{
   GP1:
	UF : 2, UT : 1,		CONFIG : 'N U T, 0, 0, 0',
	X =   -35.000  mm,	Y =     -.001  mm,	Z =     0.000  mm,
	W =     -.479 deg,	P =   -49.000 deg,	R =      .188 deg
};
P[10]{
   GP1:
	UF : 2, UT : 1,		CONFIG : 'N U T, 0, 0, 0',
	X =   -25.275  mm,	Y =    -1.251  mm,	Z =      .320  mm,
	W =     -.473 deg,	P =   -47.745 deg,	R =      .184 deg
};
P[11]{
   GP1:
	UF : 2, UT : 1,		CONFIG : 'N U T, 0, 0, 0',
	X =   -62.220  mm,	Y =     -.093  mm,	Z =    33.882  mm,
	W =     -.473 deg,	P =   -47.745 deg,	R =      .184 deg
};
P[12]{
   GP1:
	UF : 2, UT : 1,		CONFIG : 'N U T, 0, 0, 0',
	X =     1.000  mm,	Y =     -.000  mm,	Z =      .000  mm,
	W =     -.414 deg,	P =   -38.980 deg,	R =      .152 deg
};
P[13]{
   GP1:
	UF : 2, UT : 1,		CONFIG : 'N U T, 0, 0, 0',
	X =  -162.643  mm,	Y =    -1.237  mm,	Z =    32.863  mm,
	W =     -.454 deg,	P =   -48.443 deg,	R =      .179 deg
};
P[14]{
   GP1:
	UF : 0, UT : 1,		CONFIG : 'N U T, 0, 0, 0',
	X =   -44.339  mm,	Y =  1134.278  mm,	Z =  -109.089  mm,
	W =     -.452 deg,	P =   -48.443 deg,	R =    90.179 deg
};
/END
