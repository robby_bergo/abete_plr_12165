/PROG  PL_PRELI
/ATTR
OWNER		= MNEDITOR;
COMMENT		= "Prelievo Grezzo";
PROG_SIZE	= 2045;
CREATE		= DATE 07-11-07  TIME 22:58:34;
MODIFIED	= DATE 13-04-12  TIME 01:04:58;
FILE_NAME	= ;
VERSION		= 0;
LINE_COUNT	= 81;
MEMORY_SIZE	= 2505;
PROTECT		= READ_WRITE;
TCD:  STACK_SIZE	= 0,
      TASK_PRIORITY	= 50,
      TIME_SLICE	= 0,
      BUSY_LAMP_OFF	= 0,
      ABORT_REQUEST	= 0,
      PAUSE_REQUEST	= 0;
DEFAULT_GROUP	= 1,*,*,*,*;
CONTROL_CODE	= 00000000 00000000;
/MN
   1:  !--------------------------- 0B ;
   2:  !Prelievo pezzo ;
   3:  !------------------------------- ;
   4:  UFRAME_NUM=1 ;
   5:  UTOOL_NUM=1 ;
   6:   ;
   7:  IF RO[3:APRI PZ-1]=OFF,CALL P1_APRI ;
   8:   ;
   9:  JMP LBL[10] ;
  10:  !------------------------------- ;
  11:J P[1] 100% FINE    ;
  12:  LBL[10] ;
  13:J P[2] 100% FINE    ;
  14:   ;
  15:  LBL[1] ;
  16:  !------------------------------- ;
  17:  PR[51:*]=PR[49:Zero XYZ NUT]    ;
  18:  PR[51,1:*]=R[22:OFFSET Riga X]    ;
  19:  PR[51,2:*]=R[23:OFFSET Colonna Y]+120    ;
  20:  PR[51,3:*]=R[24:OFFSET IN Z]+150    ;
  21:  PR[51,5:*]=(-20)    ;
  22:  !------------------------------- ;
  23:  PR[52:*]=PR[49:Zero XYZ NUT]    ;
  24:  PR[52,1:*]=R[22:OFFSET Riga X]    ;
  25:  PR[52,2:*]=R[23:OFFSET Colonna Y]    ;
  26:  PR[52,3:*]=R[24:OFFSET IN Z]    ;
  27:  !------------------------------- ;
  28:  PR[50:*]=PR[49:Zero XYZ NUT]    ;
  29:  PR[50,1:*]=R[22:OFFSET Riga X]    ;
  30:  PR[50,2:*]=R[23:OFFSET Colonna Y]+2    ;
  31:  PR[50,3:*]=(-5)    ;
  32:   ;
  33:  PR[53:*]=PR[49:Zero XYZ NUT]    ;
  34:  PR[53,1:*]=R[22:OFFSET Riga X]    ;
  35:  PR[53,2:*]=R[23:OFFSET Colonna Y]+1    ;
  36:  PR[53,3:*]=3    ;
  37:  !------------------------------- ;
  38:   ;
  39:L PR[1:Prelievo Pz] 2000mm/sec CNT10 Offset,PR[51:*]    ;
  40:L PR[1:Prelievo Pz] 1000mm/sec FINE Offset,PR[52:*]    ;
  41:   ;
  42:L PR[1:Prelievo Pz] 100mm/sec CNT10 Offset,PR[53:*]    ;
  43:  SOFTFLOAT[1] ;
  44:  !------------------------------- ;
  45:  !PUNTO FINALE ;
  46:L PR[1:Prelievo Pz] 100mm/sec FINE Offset,PR[50:*]    ;
  47:  !------------------------------- ;
  48:   ;
  49:  SOFTFLOAT END ;
  50:  ! Chiudo ;
  51:  CALL P1_CHIUD    ;
  52:  DO[12606]=ON ;
  53:  WAIT    .20(sec) ;
  54:  SOFTFLOAT END ;
  55:   ;
  56:   ;
  57:L PR[1:Prelievo Pz] 500mm/sec CNT10 Offset,PR[52:*] ACC40    ;
  58:L PR[1:Prelievo Pz] 1000mm/sec CNT100 Offset,PR[51:*]    ;
  59:   ;
  60:  IF RI[2:P1 Chiusa]=OFF,JMP LBL[2] ;
  61:  R[3:PRESA NULLA]=R[3:PRESA NULLA]+1    ;
  62:  CALL P1_APRI    ;
  63:  DO[12606]=OFF ;
  64:  CALL ARRAY2_A(3) ;
  65:  CALL ARRAY2_A(2) ;
  66:  IF R[28:PALLET  FINITO]<>1,JMP LBL[1] ;
  67:  LBL[2] ;
  68:  IF RI[1:P1 APERTA]=ON,JMP LBL[3] ;
  69:   ;
  70:   ;
  71:J P[2] 100% FINE    ;
  72:J P[1] 100% FINE    ;
  73:   ;
  74:  R[1:CONTA PEZZI TOT]=R[1:CONTA PEZZI TOT]+1    ;
  75:  R[36:CNT_PZ]=R[36:CNT_PZ]+1    ;
  76:   ;
  77:  END ;
  78:   ;
  79:  LBL[3] ;
  80:J P[2] 100% FINE    ;
  81:J P[3] 100% FINE    ;
/POS
P[1]{
   GP1:
	UF : 1, UT : 1,		CONFIG : 'F U T, 0, 0, 0',
	X =   199.584  mm,	Y =    46.367  mm,	Z =   394.057  mm,
	W =      .149 deg,	P =   -65.406 deg,	R =    89.481 deg
};
P[2]{
   GP1:
	UF : 1, UT : 1,		CONFIG : 'F U T, 0, 0, 0',
	X =   159.840  mm,	Y =   129.407  mm,	Z =   305.032  mm,
	W =     1.007 deg,	P =   -66.356 deg,	R =    88.861 deg
};
P[3]{
   GP1:
	UF : 1, UT : 1,		CONFIG : 'F U T, 0, 0, 0',
	X =   199.584  mm,	Y =    46.367  mm,	Z =   394.057  mm,
	W =      .149 deg,	P =   -65.406 deg,	R =    89.481 deg
};
/END
