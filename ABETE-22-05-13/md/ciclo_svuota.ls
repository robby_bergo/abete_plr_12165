/PROG  CICLO_SVUOTA
/ATTR
OWNER		= MNEDITOR;
COMMENT		= "CICLO_SVUOTA";
PROG_SIZE	= 1309;
CREATE		= DATE 08-01-26  TIME 00:07:06;
MODIFIED	= DATE 13-05-22  TIME 18:49:38;
FILE_NAME	= ;
VERSION		= 0;
LINE_COUNT	= 40;
MEMORY_SIZE	= 1837;
PROTECT		= READ_WRITE;
TCD:  STACK_SIZE	= 0,
      TASK_PRIORITY	= 50,
      TIME_SLICE	= 0,
      BUSY_LAMP_OFF	= 0,
      ABORT_REQUEST	= 0,
      PAUSE_REQUEST	= 0;
DEFAULT_GROUP	= 1,*,*,*,*;
CONTROL_CODE	= 00000000 00000000;
/MN
   1:  !------------------------------- ;
   2:  !Ciclo Svuota  ;
   3:  !------------------------------- ;
   4:  !Ciclo Scarico MU ;
   5:  CALL MU_SCARICO    ;
   6:  !-------------------------------- ;
   7:  !Richiesta Pezzo Campione ;
   8:   ;
   9:  IF (!F[6:Rich. Campione]),JMP LBL[11] ;
  10:  CALL CAMPIONE    ;
  11:  CALL HOME_RBT    ;
  12:  DO[11019]=ON ;
  13:  DO[11018]=ON ;
  14:  PAUSE ;
  15:  JMP LBL[4] ;
  16:  LBL[11] ;
  17:   ;
  18:  !------------------------------ ;
  19:  !Deposito Pezzo ;
  20:  CALL ARRAY2_A(2) ;
  21:  CALL PL_DEPOS    ;
  22:  LBL[4] ;
  23:   ;
  24:  !-- MORSA IN PRELIEVO---------- ;
  25:  UFRAME_NUM=0 ;
  26:  UTOOL_NUM=1 ;
  27:J P[1] 50% CNT50    ;
  28:  !------------------------------- ;
  29:  ! FINE CARCO MU ;
  30:  DO[75:RBT FUORI ING]=ON ;
  31:   ;
  32:  R[30:SETTO RIEMP1]=0    ;
  33:  !------------------------------- ;
  34:  !Richiesta pause ;
  35:  CALL VRF_STOP    ;
  36:   ;
  37:  !------------------------------- ;
  38:  !Fine ciclo robot ;
  39:  CALL HOME_RBT    ;
  40:  CALL HOME_CAS    ;
/POS
P[1]{
   GP1:
	UF : 0, UT : 1,		CONFIG : 'N U T, 0, 0, 0',
	X =      .027  mm,	Y =  -383.922  mm,	Z =    31.904  mm,
	W =      .004 deg,	P =      .001 deg,	R =   -90.003 deg
};
/END
