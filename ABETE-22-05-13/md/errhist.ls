ERRHIST.LS     Robot Name ROBOT 22-MAY-13 22:40     

F Number:F00000    
 VERSION: HandlingTool          
$VERSION: V7.7071      10/10/2012 
$FEATURE[1]: HandlingTool          
$FEATURE[2]: English Dictionary    
$FEATURE[3]: AA Vision Mastering   
$FEATURE[4]: Analog I/O            
$FEATURE[5]: Auto Software Update  
$FEATURE[6]: Automatic Backup      
$FEATURE[7]: Background Editing    
$FEATURE[8]: Camera I/F            
$FEATURE[9]: Cell I/O              
$FEATURE[10]: Common shell          
$FEATURE[11]: Common shell core     
$FEATURE[12]: Common softpanel      
$FEATURE[13]: Common style select   
$FEATURE[14]: Condition Monitor     
$FEATURE[15]: Control Reliable      
$FEATURE[16]: Diagnostic log        
$FEATURE[17]: Dual Check Safety UIF 
$FEATURE[18]: Enhanced Rob Serv Req 
$FEATURE[19]: Enhanced User Frame   
$FEATURE[20]: Ext. DIO Config       
$FEATURE[21]: Extended Error Log    
$FEATURE[22]: External DI BWD       
$FEATURE[23]: FCTN Menu Save        
$FEATURE[24]: FTP Interface         
$FEATURE[25]: Group Mask Exchange   
$FEATURE[26]: High-Speed Skip       
$FEATURE[27]: Host Communications   
$FEATURE[28]: Hour Meter            
$FEATURE[29]: I/O Interconnect 2    
$FEATURE[30]: Incr Instruction      
$FEATURE[31]: KAREL Cmd. Language   
$FEATURE[32]: KAREL Run-Time Env    
$FEATURE[33]: Kernel + Basic S/W    
$FEATURE[34]: License Checker       
$FEATURE[35]: LogBook(System)       
$FEATURE[36]: MACROs, Skip/Offset   
$FEATURE[37]: MH Core               
$FEATURE[38]: MechStop Protection   
$FEATURE[39]: Mirror Shift          
$FEATURE[40]: Mixed logic           
$FEATURE[41]: Mode Switch           
$FEATURE[42]: Motion Diag. Core     
$FEATURE[43]: Motion logger         
$FEATURE[44]: Multi-Tasking         
$FEATURE[45]: Position Registers    
$FEATURE[46]: Print Function        
$FEATURE[47]: Prog Num Selection    
$FEATURE[48]: Program Adjust        
$FEATURE[49]: Program Shift         
$FEATURE[50]: Program Status        
$FEATURE[51]: RDM Robot Discovery   
$FEATURE[52]: Robot Service Request 
$FEATURE[53]: Robot Servo Code      
$FEATURE[54]: SNPX basic            
$FEATURE[55]: Shift Library         
$FEATURE[56]: Shift and Mirror Lib  
$FEATURE[57]: Soft Parts in VCCM    
$FEATURE[58]: TCP Auto Set          
$FEATURE[59]: TCP/IP Interface      
$FEATURE[60]: TMILIB Interface      
$FEATURE[61]: TP Menu Accounting    
$FEATURE[62]: TPTX                  
$FEATURE[63]: Telnet Interface      
$FEATURE[64]: Tool Offset           
$FEATURE[65]: Unexcepted motn Check 
$FEATURE[66]: User Frame            
$FEATURE[67]: Vision Core           
$FEATURE[68]: Vision Library        
$FEATURE[69]: Vision SP CSXC        
$FEATURE[70]: Vision Shift Tool     
$FEATURE[71]: Web Server            
$FEATURE[72]: Web Svr Enhancements  
$FEATURE[73]: iPendant              
$FEATURE[74]: iPendant Grid Display 
$FEATURE[75]: iPendant Setup        
$FEATURE[76]: Independent Axes      
$FEATURE[77]: M-6iB6S AM100iB6S iC  
$FEATURE[78]: Ascii Upload          
$FEATURE[79]: AutoSingularityAvoidM 
$FEATURE[80]: CE Mark               
$FEATURE[81]: Collision Guard       
$FEATURE[82]: Collision Guard Pack  
$FEATURE[83]: Constant Path         
$FEATURE[84]: Cycle Time Priority   
$FEATURE[85]: DHCP                  
$FEATURE[86]: Domain Name Serv.     
$FEATURE[87]: Extended User Frames  
$FEATURE[88]: FRL Params            
$FEATURE[89]: HMI Device (SNPX)     
$FEATURE[90]: High perf. softfloat  
$FEATURE[91]: Internet Conn/Custo   
$FEATURE[92]: Multi-Group Motion    
$FEATURE[93]: PC Interface          
$FEATURE[94]: PMC(FAPT Ladder)      
$FEATURE[95]: PROFIBUS              
$FEATURE[96]: PROFIBUS-DP(Master)   
$FEATURE[97]: Password Protection   
$FEATURE[98]: SNTP Client           
$FEATURE[99]: Soft Float            
$FEATURE[100]: Space Check           
$FEATURE[101]: USB port on iPendant  
$FEATURE[102]: YELLOW BOX            
$FEATURE[103]: Arc Advisor           
$FEATURE[104]: Auto SingularityAvoid 
$FEATURE[105]: Aux Servo Code        
$FEATURE[106]: Cycle time Opt.       
$FEATURE[107]: Disp 2nd analog port  
$FEATURE[108]: EMAIL Enhancements    
$FEATURE[109]: Email Client          
$FEATURE[110]: Enhanced T1 Mode      
$FEATURE[111]: Func stup             
$FEATURE[112]: HTTP Proxy Svr        
$FEATURE[113]: IntelligentTP PC I/F  
$FEATURE[114]: JPN ARCPSU PRM        
$FEATURE[115]: PC Send Macros        
$FEATURE[116]: Real Simple Syn.(RSS) 
$FEATURE[117]: Requires CP           
$FEATURE[118]: Robot Library Setup   
$FEATURE[119]: SMB Client            
$FEATURE[120]: SSPC error text       
$FEATURE[121]: Socket Messaging      
$FEATURE[122]: TCPP Extention        
$FEATURE[123]: istdpnl               
$FEATURE[124]: SHADOW MOVE TO CMOS O 
$FEATURE[125]: SMB: vision interacti 
$FEATURE[126]: get_var fails to get  
$FEATURE[127]: Arc End STEP Hold Bus 
$FEATURE[128]: CVIS UPDATE IRTORCHMA 
$FEATURE[129]: CVIS UPDATE WELDTIP S 
$FEATURE[130]: CVIS: R741 should be  
$FEATURE[131]: BACKGROUND EDIT SHADO 
$FEATURE[132]: REPTCD CAN CRASH FRVR 
$FEATURE[133]: CVIS: Crashes 2-D Bar 
$FEATURE[134]: $FNO NOT RESTORED AUT 
$FEATURE[135]: JOINT QUICK STOP FLEN 
$FEATURE[136]: Local Hold TIMQ Adjus 
$FEATURE[137]: FPLN:  Support facepl 
$FEATURE[138]: FMD DEVICE ASSERT WIT 
$FEATURE[139]: KAREL CANNOT ACCESS M 
$FEATURE[140]: Joint Quick Stop FLEN 
$FEATURE[141]: USB insert & removal  
$FEATURE[142]: SMB NULL POINTER FIX  
$FEATURE[143]: MACHINE TOOL DEMO OPT 
$FEATURE[144]: PMC ETHERNET SUPPORT  
$FEATURE[145]: CPPOSTPC CAN CRASH DU 
$FEATURE[146]: CVIS Add reader visio 
$FEATURE[147]: $PASSWORD.$AUTOLOGIN  
$FEATURE[148]: CP:fix CPMO-046 issue 
$FEATURE[149]: J AUTO-T1 CPMO-130    
$FEATURE[150]: CVIS SET VARIABLES TO 
$FEATURE[151]: CVIS: 3D multi-view d 
$FEATURE[152]: Increase Number of FD 
$FEATURE[153]: CP: fix OS-144 with s 
$FEATURE[154]: Robot Settings are lo 
$FEATURE[155]: Maximum number of ele 
$FEATURE[156]: CP WAIT Mode 3 CNT0 T 
$FEATURE[157]: Mirror Image Shift ca 
$FEATURE[158]: SPOT:When viewing STY 
$FEATURE[159]: Attempt to do GET_VAR 
$FEATURE[160]: VMGR LOAD REALLOCATE  
$FEATURE[161]: Arc Production Monito 
$FEATURE[162]: SELECT FILTERING IMPR 
$FEATURE[163]: REMOVE KAREL PROGRAM  
$FEATURE[164]: RIPE:STARTUP MAIN-MAI 
$FEATURE[165]: ASCII UPLOAD OF LINE  
$FEATURE[166]: PTTB: New ML Setup me 
$FEATURE[167]: USB: memory allocatio 
$FEATURE[168]: Torch Angle supports  
$FEATURE[169]: Enhanced thickness ch 
$FEATURE[170]: Support program touch 
$FEATURE[171]: iRCalibration Signatu 
$FEATURE[172]: Support Thresh123     
$FEATURE[173]: CP:fix MOTN-110 by ma 
$FEATURE[174]: HOST : MSG_PING asser 
$FEATURE[175]: CP: All zero move tim 
$FEATURE[176]: CVIS: Improve 2D barc 
$FEATURE[177]: SOMETIMES ON STARTUP  
$FEATURE[178]: SREG: Support string  
$FEATURE[179]: SREG: KANJI string no 
$FEATURE[180]: HOST : SMB fails in c 
$FEATURE[181]: cvis vision data file 
$FEATURE[182]: PROGRAM INISITALIZATI 
$FEATURE[183]: Arc weave estop drift 
$FEATURE[184]: CVIS: Add minimum rec 
$FEATURE[185]: CVIS: Problem with ro 
$FEATURE[186]: CVIS: Selected image  
$FEATURE[187]: ASCII Load fails when 
$FEATURE[188]: CVIS Allow user to se 
$FEATURE[189]: SYSTEM FILES CAN GET  
$FEATURE[190]: CVIS Include Error Re 
$FEATURE[191]: CPMO-014 with Group m 
$FEATURE[192]: Warning Prompt Box fo 
$FEATURE[193]: OVERWRITE OF LS FILE  
$FEATURE[194]: Fix upgrade-related d 
$FEATURE[195]: Switching between IND 
$FEATURE[196]: GigE snap causing spo 
$FEATURE[197]: TPDRAM DOES NOT HANDL 
$FEATURE[198]: CVIS GET_READING not  
$FEATURE[199]: GigE Interoperability 
$FEATURE[200]: HOST: Handle leading  
$FEATURE[201]: CVIS: Image to points 
$FEATURE[202]: PORT FAST RIPE TO V7. 
$FEATURE[203]: CGTP: piMenus does no 
$FEATURE[204]: Line-Remark Paste RM- 
$FEATURE[205]: SHADOW PATH SAVE LOOS 
$FEATURE[206]: Password Config - GLA 
$FEATURE[207]: SPOT: Add plib suppor 
$FEATURE[208]: Ethernet Line Trackin 
$FEATURE[209]: Weave Hold CPMO-073   
$FEATURE[210]: CP: arc weave plan er 
$FEATURE[211]: HTTP: Long delays wit 
$FEATURE[212]: PTTB: Payload[2] not  
$FEATURE[213]: T1: group slowdown    
$FEATURE[214]: DISABLE CPMEMCHK EXCE 
$FEATURE[215]: CP: MF + Local hold C 
$FEATURE[216]: CVIS: UIF BUG FIX     
$FEATURE[217]: DOT PETTERN BUG       
$FEATURE[218]: EXPANSION DIGITAL I/O 
$FEATURE[219]: CVIS CCRG ENHANCEMENT 
$FEATURE[220]: MLOCK CHK WITH GUNCHG 
$FEATURE[221]: EXTEND PROFILE STEPS  
$FEATURE[222]: VISION MEMORYLEAK FIX 
$FEATURE[223]: AUTO CALC WRDN RATIO  
$FEATURE[224]: CVIS:SPEC CHANGE OF V 
$FEATURE[225]: VMASTER FIX           
$FEATURE[226]: GRID PATTERN BUG FIX  
$FEATURE[227]: PMC ETHERNET SUPPORT  
$FEATURE[228]: DISPENSE INST IMPROVE 
$FEATURE[229]: BROWSER TOOL RMV ADD  
$FEATURE[230]: FIX DCS COUNT3 ALARM  
$FEATURE[231]: SVGN EARLY WELD INIT  
$FEATURE[232]: STOP PATTERN DISPLAY  
$FEATURE[233]: IMP SPD SEARCH ALARMS 
$FEATURE[234]: TOUCHUP AND OFFSET    
$FEATURE[235]: ADD VISION ERROR CODE 
$FEATURE[236]: WRONG CURSOR POSITION 
$FEATURE[237]: FIX SVGN-158          
$FEATURE[238]: RSR IS NOT USABLE     
$FEATURE[239]: FIX GUNTCHUP FUNCTION 
$FEATURE[240]: TOUCHUP WITH OFFSET   
$FEATURE[241]: FIX DB BUSY RUNNING   
$FEATURE[242]: FIX BRKCTRL PROBLEM   
$FEATURE[243]: FIX PMC ETHERNET      
$FEATURE[244]: VISION AXIS MASTER    
$FEATURE[245]: FIX VMASTER ISSUE     
$FEATURE[246]: IMPROVE FLP PACKETS   
$FEATURE[247]: IMPROVED CTSI MASK    
$FEATURE[248]: CIPSAFETY CLOCK CHECK 
$FEATURE[249]: FIX LOADING DB SYSVAR 
$FEATURE[250]: ENHANCED SVGN TWD     
$FEATURE[251]: DCS INVALID ZONE      
$FEATURE[252]: FIX BUSY WITH DUALGUN 
$FEATURE[253]: DCS TOOL FRAME FIX    
$FEATURE[254]: PR INDEX FOR TOUCHUP  
$FEATURE[255]: WRONG UF FOR PALLET   
$FEATURE[256]: FIX SRVO117 AT STRTUP 
$FEATURE[257]: FIX FALSE INTP247     
$FEATURE[258]: PLID SUPPORT HIGH ACC 
$FEATURE[259]: SUPPORT SNPX COMMAND  
$FEATURE[260]: FIX CANNOT SAVE VR FI 
$FEATURE[261]: FIX VISION GRID DETEC 
$FEATURE[262]: FIX ARC INSTRUCTION   
$FEATURE[263]: FIX CALL INSTRUCTION  
$FEATURE[264]: FIX FALSE CPMO-073    
$FEATURE[265]: GRID DETECT BUG FIX   

 MOTION PARAMETERS:: 
 ROBOT: M-6iB/6S iC 
 SERVO ID: V22.01   
 Min Cycle: V3.00 
 Min Cart: V3.00 
GRP:  1 AXS:  1 MTR_ID aiF4/4000 40A 
                MTR_INF: H1 DSP1-L 
                SRV_ID: P02.01
GRP:  1 AXS:  2 MTR_ID aiF4/4000 40A 
                MTR_INF: H2 DSP1-M 
                SRV_ID: P02.01
GRP:  1 AXS:  3 MTR_ID aiF1/5000 20A 
                MTR_INF: H3 DSP1-J 
                SRV_ID: P02.01
GRP:  1 AXS:  4 MTR_ID aiF1/5000 20A 
                MTR_INF: H4 DSP1-K 
                SRV_ID: P02.01
GRP:  1 AXS:  5 MTR_ID biS0.5/6000 20A 
                MTR_INF: H5 DSP2-L 
                SRV_ID: P02.01
GRP:  1 AXS:  6 MTR_ID biS0.4/5000 20A 
                MTR_INF: H6 DSP2-M 
                SRV_ID: P02.01
GRP:  2 AXS:  1 MTR_ID aiS2/5000 20A 
                MTR_INF: H  DSP - 
                SRV_ID: P00.39
GRP:  2 AXS:  2 MTR_ID aiS4/5000 20A 
                MTR_INF: H  DSP - 
                SRV_ID: P00.39
 PWR: 37939 SRV 4503 RUN: 4121 WAIT 1721
$SCR.$NUM_GROUP  : 2
$SCR.$NUM_TOT_AXS: 8
$SCR.$MAXNUMTASK : 4
$SCR.$NUM_GROUP  : 2
$SCR.$MAXNUMUFRAM: 9
$SCR.$MAXNUMUTOOL: 10
$SCR.$NUM_GROUP  : 2
PAYLOAD          : 6

 SETUP PARAMETERS:: 
$MAXRERGNUM     : 700
$MAXPRERGNUM    : 100
HARDWARE CONFIGURATION::

         Slot  ID  FC  OP

 MEMORY INFORMATION:: 
Memory     Total      Free   Largest      Used
-----------------------------------------------
  PERM    1ee800     974d8     9668c    157328
   TPP     af000     99dbc     99158     15244
SYSTEM    6ab801       e30       e30    6aa9d1
  TEMP   14e419c    923f54    56887c    bc0248
  CMOS    200000
  DRAM   2000000
  FROM   2000000
ETHERNET CONFIGURATION::
$HOSTNAME  : ROBOT 
Router Name: ROUTER 
Hardware address :  
Subnet mask :  
Server [1]:         FTP state: 3 
Server [2]:         FTP state: 3 
