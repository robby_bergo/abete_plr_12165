/PROG  CAMPIONE
/ATTR
OWNER		= MNEDITOR;
COMMENT		= "Deposito Campion";
PROG_SIZE	= 1405;
CREATE		= DATE 08-01-01  TIME 14:00:00;
MODIFIED	= DATE 13-04-12  TIME 17:56:14;
FILE_NAME	= ;
VERSION		= 0;
LINE_COUNT	= 49;
MEMORY_SIZE	= 1873;
PROTECT		= READ_WRITE;
TCD:  STACK_SIZE	= 0,
      TASK_PRIORITY	= 50,
      TIME_SLICE	= 0,
      BUSY_LAMP_OFF	= 0,
      ABORT_REQUEST	= 0,
      PAUSE_REQUEST	= 0;
DEFAULT_GROUP	= 1,*,*,*,*;
CONTROL_CODE	= 00000000 00000000;
/MN
   1:  !-------------------------- 11G ;
   2:  !Deposito pezzo Campione ;
   3:  !------------------------------- ;
   4:  UFRAME_NUM=3 ;
   5:  UTOOL_NUM=1 ;
   6:   ;
   7:  IF RI[1:P1 APERTA]=ON,CALL P1_CHIUD ;
   8:   ;
   9:  LBL[2] ;
  10:  IF R[31:CAMPIONE]<>1,JMP LBL[1] ;
  11:  CALL MESSAGGI(17) ;
  12:  PAUSE ;
  13:  CALL MESSAGGI(1) ;
  14:  JMP LBL[2] ;
  15:  LBL[1] ;
  16:   ;
  17:J P[1] 50% CNT60    ;
  18:J P[2] 50% CNT50    ;
  19:J P[3] 50% CNT50    ;
  20:   ;
  21:  PR[51:*]=PR[49:Zero XYZ NUT]    ;
  22:  PR[51,1:*]=0    ;
  23:  PR[51,2:*]=0    ;
  24:  PR[51,3:*]=200    ;
  25:   ;
  26:  PR[52:*]=PR[49:Zero XYZ NUT]    ;
  27:  PR[52,1:*]=(-20)    ;
  28:  PR[52,2:*]=0    ;
  29:  PR[52,3:*]=80    ;
  30:  PR[52,5:*]=20    ;
  31:   ;
  32:L PR[3:Campione] 500mm/sec CNT10 Offset,PR[51:*]    ;
  33:L PR[3:Campione] 500mm/sec CNT10 Offset,PR[52:*]    ;
  34:  !------------------------------- ;
  35:  !PUNTO FINALE ;
  36:L PR[3:Campione] 200mm/sec FINE    ;
  37:  !------------------------------- ;
  38:  CALL P1_APRI    ;
  39:   ;
  40:L PR[3:Campione] 500mm/sec CNT50 Offset,PR[51:*]    ;
  41:J P[3] 40% CNT50    ;
  42:J P[2] 100% CNT50    ;
  43:J P[1] 100% CNT60    ;
  44:   ;
  45:  CALL ARRAY2_A(3) ;
  46:   ;
  47:  F[6:Rich. Campione]=(OFF) ;
  48:  R[31:CAMPIONE]=1    ;
  49:   ;
/POS
P[1]{
   GP1:
	UF : 3, UT : 1,		CONFIG : 'N U T, 0, 0, 0',
	X =  -497.552  mm,	Y =  -207.022  mm,	Z =   332.457  mm,
	W =      .003 deg,	P =      .001 deg,	R =     -.002 deg
};
P[2]{
   GP1:
	UF : 3, UT : 1,	
	J1=  -105.734 deg,	J2=   -20.075 deg,	J3=   -32.273 deg,
	J4=    61.780 deg,	J5=    16.385 deg,	J6=   -54.792 deg
};
P[3]{
   GP1:
	UF : 3, UT : 1,		CONFIG : 'F U T, 0, 0, 0',
	X =  -115.501  mm,	Y =  -327.297  mm,	Z =   260.281  mm,
	W =      .993 deg,	P =   -66.450 deg,	R =    -1.086 deg
};
/END
