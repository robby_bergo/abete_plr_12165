/PROG  MORSA_DP_RIEMP
/ATTR
OWNER		= MNEDITOR;
COMMENT		= "DEPO_MORSA";
PROG_SIZE	= 1769;
CREATE		= DATE 13-04-10  TIME 19:03:02;
MODIFIED	= DATE 13-04-10  TIME 19:03:02;
FILE_NAME	= MORSA_DP;
VERSION		= 0;
LINE_COUNT	= 71;
MEMORY_SIZE	= 2129;
PROTECT		= READ_WRITE;
TCD:  STACK_SIZE	= 0,
      TASK_PRIORITY	= 50,
      TIME_SLICE	= 0,
      BUSY_LAMP_OFF	= 0,
      ABORT_REQUEST	= 0,
      PAUSE_REQUEST	= 0;
DEFAULT_GROUP	= 1,*,*,*,*;
CONTROL_CODE	= 00000000 00000000;
/APPL

AUTO_SINGULARITY_HEADER;
  ENABLE_SINGULARITY_AVOIDANCE   : FALSE;
/MN
   1:  UFRAME_NUM=0 ;
   2:  UTOOL_NUM=1 ;
   3:   ;
   4:  WAIT DI[137:PRES.PZ.ATTREZ.]=ON    ;
   5:   ;
   6:J P[1] 50% CNT50    ;
   7:J P[2] 50% CNT50    ;
   8:J P[3] 50% CNT50    ;
   9:   ;
  10:  PR[51:*]=PR[49:Zero XYZ NUT]    ;
  11:  PR[51,1:*]=100    ;
  12:  PR[51,2:*]=0    ;
  13:  PR[51,3:*]=(-100)    ;
  14:   ;
  15:  PR[52:*]=PR[49:Zero XYZ NUT]    ;
  16:  PR[52,1:*]=0    ;
  17:  PR[52,2:*]=0    ;
  18:  PR[52,3:*]=(-50)    ;
  19:   ;
  20:  PR[53:*]=PR[49:Zero XYZ NUT]    ;
  21:  PR[53,1:*]=0    ;
  22:  PR[53,2:*]=0    ;
  23:  PR[53,3:*]=100    ;
  24:   ;
  25:J P[5] 50% CNT50    ;
  26:   ;
  27:L P[7] 800mm/sec CNT10 Tool_Offset,PR[53:*]    ;
  28:L P[7] 100mm/sec FINE    ;
  29:   ;
  30:  WAIT DI[137:PRES.PZ.ATTREZ.]=ON    ;
  31:  CALL P1_CHIUD    ;
  32:  WAIT    .50(sec) ;
  33:  DO[181:SBLOCC. MAND]=ON ;
  34:  WAIT   1.00(sec) ;
  35:   ;
  36:L P[7] 500mm/sec FINE Offset,PR[52:*]    ;
  37:   ;
  38:L P[7] 1000mm/sec CNT50 Offset,PR[51:*]    ;
  39:J P[3] 50% CNT50    ;
  40:   ;
  41:  PR[51:*]=PR[49:Zero XYZ NUT]    ;
  42:  PR[51,1:*]=100    ;
  43:  PR[51,2:*]=0    ;
  44:  PR[51,3:*]=400    ;
  45:   ;
  46:  PR[52:*]=PR[49:Zero XYZ NUT]    ;
  47:  PR[52,1:*]=0    ;
  48:  PR[52,2:*]=0    ;
  49:  PR[52,3:*]=100    ;
  50:   ;
  51:  PR[53:*]=PR[49:Zero XYZ NUT]    ;
  52:  PR[53,1:*]=0    ;
  53:  PR[53,2:*]=(-100)    ;
  54:  PR[53,3:*]=(-5)    ;
  55:   ;
  56:   ;
  57:L P[4] 1000mm/sec CNT50 Tool_Offset,PR[51:*]    ;
  58:L P[4] 500mm/sec CNT10 Offset,PR[53:*]    ;
  59:L P[4] 100mm/sec FINE    ;
  60:   ;
  61:  DO[181:SBLOCC. MAND]=OFF ;
  62:  WAIT   1.00(sec) ;
  63:  CALL P1_APRI    ;
  64:  WAIT    .50(sec) ;
  65:   ;
  66:L P[4] 500mm/sec CNT10 Tool_Offset,PR[52:*]    ;
  67:L P[4] 1000mm/sec CNT50 Tool_Offset,PR[51:*]    ;
  68:J P[3] 50% CNT50    ;
  69:J P[2] 50% CNT50    ;
  70:J P[1] 50% CNT50    ;
  71:   ;
/POS
P[1]{
   GP1:
	UF : 0, UT : 1,		CONFIG : 'N U T, 0, 0, 0',
	X =      .027  mm,	Y =  -383.922  mm,	Z =    31.904  mm,
	W =      .004 deg,	P =      .001 deg,	R =   -90.003 deg
};
P[2]{
   GP1:
	UF : 0, UT : 1,		CONFIG : 'N U T, 0, 0, 0',
	X =    -8.087  mm,	Y =  -303.235  mm,	Z =   237.398  mm,
	W =    -1.311 deg,	P =   -33.079 deg,	R =   -90.960 deg
};
P[3]{
   GP1:
	UF : 0, UT : 1,		CONFIG : 'N U T, 0, 0, 0',
	X =     2.146  mm,	Y =   285.726  mm,	Z =   226.632  mm,
	W =    -1.386 deg,	P =   -29.882 deg,	R =    90.237 deg
};
P[4]{
   GP1:
	UF : 0, UT : 1,		CONFIG : 'N U T, 0, 0, 0',
	X =   -43.831  mm,	Y =   961.943  mm,	Z =  -285.577  mm,
	W =    -1.016 deg,	P =   -47.466 deg,	R =    90.708 deg
};
P[5]{
   GP1:
	UF : 0, UT : 1,		CONFIG : 'N U T, 0, 0, 0',
	X =  -412.386  mm,	Y =    26.908  mm,	Z =   607.597  mm,
	W =   179.582 deg,	P =   -25.140 deg,	R =     -.276 deg
};
P[7]{
   GP1:
	UF : 0, UT : 1,		CONFIG : 'N U T, 0, 0, 0',
	X =  -523.331  mm,	Y =    26.907  mm,	Z =   709.517  mm,
	W =   179.802 deg,	P =   -53.482 deg,	R =     -.420 deg
};
/END
