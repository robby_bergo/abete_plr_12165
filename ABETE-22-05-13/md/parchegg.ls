/PROG  PARCHEGG
/ATTR
OWNER		= MNEDITOR;
COMMENT		= "RBT in parcheggi";
PROG_SIZE	= 732;
CREATE		= DATE 07-11-15  TIME 22:26:38;
MODIFIED	= DATE 13-04-05  TIME 17:11:50;
FILE_NAME	= ;
VERSION		= 0;
LINE_COUNT	= 35;
MEMORY_SIZE	= 1240;
PROTECT		= READ_WRITE;
TCD:  STACK_SIZE	= 0,
      TASK_PRIORITY	= 50,
      TIME_SLICE	= 0,
      BUSY_LAMP_OFF	= 0,
      ABORT_REQUEST	= 0,
      PAUSE_REQUEST	= 0;
DEFAULT_GROUP	= *,1,*,*,*;
CONTROL_CODE	= 00000000 00000000;
/MN
   1:  !---------------------- Ed. 11G ;
   2:  !ROBOT IN PARCHEGGIO ;
   3:  !------------------------------- ;
   4:   ;
   5:  CALL HOME_RBT    ;
   6:  WAIT DO[11501]=ON    ;
   7:   ;
   8:  !Porto il ROBOT ;
   9:  !IN POSIZIONE ALTO ;
  10:   ;
  11:  PR[50:*]=LPOS    ;
  12:   ;
  13:  PR[51:*]=PR[49:Zero XYZ NUT]    ;
  14:  PR[51,1:*]=PR[50,1:*]    ;
  15:  PR[51,2:*]=2000    ;
  16:   ;
  17:L PR[51:*] 1000mm/sec FINE    ;
  18:   ;
  19:  !Attivo comandi RBT ;
  20:  DO[11002]=ON ;
  21:  ! ;
  22:  LBL[1] ;
  23:  F[4:VAI PARCHEGGIO]=(OFF) ;
  24:  !Richiedo apertura porte ;
  25:  DO[11018]=ON ;
  26:  DO[11019]=ON ;
  27:   ;
  28:   ;
  29:  PAUSE ;
  30:  DO[11002]=OFF ;
  31:   ;
  32:  !Porto il ROBOT ;
  33:  !IN POSIZIONE BASSO ;
  34:  CALL HOME_CAS    ;
  35:   ;
/POS
/END
