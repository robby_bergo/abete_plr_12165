/PROG  PL_DEPOS
/ATTR
OWNER		= MNEDITOR;
COMMENT		= "Deposito Grezzo";
PROG_SIZE	= 1645;
CREATE		= DATE 13-04-05  TIME 23:44:30;
MODIFIED	= DATE 13-05-14  TIME 22:10:56;
FILE_NAME	= PL_PRELI;
VERSION		= 0;
LINE_COUNT	= 52;
MEMORY_SIZE	= 2093;
PROTECT		= READ_WRITE;
TCD:  STACK_SIZE	= 0,
      TASK_PRIORITY	= 50,
      TIME_SLICE	= 0,
      BUSY_LAMP_OFF	= 0,
      ABORT_REQUEST	= 0,
      PAUSE_REQUEST	= 0;
DEFAULT_GROUP	= 1,*,*,*,*;
CONTROL_CODE	= 00000000 00000000;
/APPL

AUTO_SINGULARITY_HEADER;
  ENABLE_SINGULARITY_AVOIDANCE   : TRUE;
/MN
   1:  !--------------------------- 0B ;
   2:  !Prelievo pezzo ;
   3:  !------------------------------- ;
   4:  UFRAME_NUM=1 ;
   5:  UTOOL_NUM=1 ;
   6:   ;
   7:  IF RI[1:P1 APERTA]=ON,CALL P1_CHIUD ;
   8:   ;
   9:  !------------------------------- ;
  10:J P[1] 100% FINE    ;
  11:J P[2] 100% FINE    ;
  12:   ;
  13:  LBL[1] ;
  14:  !------------------------------- ;
  15:  PR[51:*]=PR[49:Zero XYZ NUT]    ;
  16:  PR[51,1:*]=R[22:OFFSET Riga X]    ;
  17:  PR[51,2:*]=R[23:OFFSET Colonna Y]+150    ;
  18:  PR[51,3:*]=R[24:OFFSET IN Z]+150    ;
  19:  PR[51,5:*]=(-20)    ;
  20:  !------------------------------- ;
  21:  PR[52:*]=PR[49:Zero XYZ NUT]    ;
  22:  PR[52,1:*]=R[22:OFFSET Riga X]    ;
  23:  PR[52,2:*]=R[23:OFFSET Colonna Y]    ;
  24:  PR[52,3:*]=R[24:OFFSET IN Z]    ;
  25:  PR[52,5:*]=3    ;
  26:  !------------------------------- ;
  27:  PR[50:*]=PR[49:Zero XYZ NUT]    ;
  28:  PR[50,1:*]=R[22:OFFSET Riga X]    ;
  29:  PR[50,2:*]=R[23:OFFSET Colonna Y]+R[26:OFFSET X PR MU]    ;
  30:  PR[50,3:*]=10    ;
  31:  !------------------------------- ;
  32:   ;
  33:L PR[1:Prelievo Pz] 2000mm/sec CNT10 Offset,PR[51:*]    ;
  34:L PR[1:Prelievo Pz] 1000mm/sec FINE Offset,PR[52:*]    ;
  35:  !------------------------------- ;
  36:  !PUNTO FINALE ;
  37:L PR[1:Prelievo Pz] 100mm/sec FINE Offset,PR[50:*]    ;
  38:  !------------------------------- ;
  39:   ;
  40:  ! Chiudo ;
  41:  CALL P1_APRI    ;
  42:  WAIT    .20(sec) ;
  43:   ;
  44:L PR[1:Prelievo Pz] 500mm/sec CNT2 Offset,PR[52:*] ACC40    ;
  45:L PR[1:Prelievo Pz] 1000mm/sec CNT20 Offset,PR[51:*]    ;
  46:   ;
  47:J P[2] 100% FINE    ;
  48:J P[1] 100% FINE    ;
  49:   ;
  50:  CALL ARRAY2_A(3) ;
  51:  CALL ARRAY2_A(2) ;
  52:   ;
/POS
P[1]{
   GP1:
	UF : 1, UT : 1,		CONFIG : 'F U T, 0, 0, 0',
	X =   199.584  mm,	Y =    46.367  mm,	Z =   394.057  mm,
	W =      .149 deg,	P =   -65.406 deg,	R =    89.481 deg
};
P[2]{
   GP1:
	UF : 1, UT : 1,		CONFIG : 'F U T, 0, 0, 0',
	X =   159.840  mm,	Y =   129.407  mm,	Z =   305.032  mm,
	W =     1.007 deg,	P =   -66.356 deg,	R =    88.861 deg
};
/END
