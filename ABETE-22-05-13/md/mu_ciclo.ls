/PROG  MU_CICLO
/ATTR
OWNER		= MNEDITOR;
COMMENT		= "CICLO MU";
PROG_SIZE	= 1396;
CREATE		= DATE 07-04-17  TIME 13:33:10;
MODIFIED	= DATE 13-04-10  TIME 00:15:02;
FILE_NAME	= ;
VERSION		= 0;
LINE_COUNT	= 55;
MEMORY_SIZE	= 1724;
PROTECT		= READ_WRITE;
TCD:  STACK_SIZE	= 0,
      TASK_PRIORITY	= 50,
      TIME_SLICE	= 0,
      BUSY_LAMP_OFF	= 0,
      ABORT_REQUEST	= 0,
      PAUSE_REQUEST	= 0;
DEFAULT_GROUP	= 1,*,*,*,*;
CONTROL_CODE	= 00000000 00000000;
/MN
   1:  !--------------------- Ed. 11G ;
   2:  ! Ciclo per verifica ;
   3:  !------------------------------ ;
   4:  ! Condizioni iniziali ;
   5:  ! RBT in Auto ;
   6:  ! ;
   7:  ! Grata OK ;
   8:  ! RBT Pronto ;
   9:  DO[74:START MU]=ON ;
  10:  ! RBT Fuori Ingombro ;
  11:  DO[75:RBT FUORI ING]=ON ;
  12:   ;
  13:  !------------------------------- ;
  14:  !Dialogo RBT e MU ;
  15:  !------------------------------- ;
  16:   ;
  17:  !-- Attendo MU AUTO------ ;
  18:  WAIT DI[76:MACC.IN FUNZIONE]=ON AND DI[77:MACC.IN AUTO]=ON    ;
  19:  ! Attendo Porta Aperta ;
  20:  WAIT DI[79:PORTA AP.MU]=ON    ;
  21:  ! OK al movimento ;
  22:  WAIT DI[67:ASSI FUOR.ING]=ON    ;
  23:  ! Attendo Richiesta Scarico/Car ;
  24:  WAIT DI[65:MAC.PRONTA M20]=ON    ;
  25:  WAIT    .50(sec) ;
  26:  !-- RBT Fuori ingombro ---------- ;
  27:  DO[75:RBT FUORI ING]=OFF ;
  28:   ;
  29:  !-- MORSA IN DEPOSITO------------ ;
  30:  CALL MORSA_DP    ;
  31:   ;
  32:  !-- SCARICO PEZZO----------- ;
  33:  CALL MU_SCARICO    ;
  34:   ;
  35:  !-- DEPO PEZZO PLT----------- ;
  36:  CALL PL_DEPOS    ;
  37:   ;
  38:  !-- PRELIEVO PEZZO PLT----------- ;
  39:  CALL PL_PRELI    ;
  40:   ;
  41:  !-- CARICO PEZZO----------- ;
  42:  CALL MU_CARICO    ;
  43:   ;
  44:  !-- MORSA IN PRELIEVO---------- ;
  45:  CALL MORSA_PR    ;
  46:   ;
  47:  WAIT   1.00(sec) ;
  48:  !-- Robot fuori ----------------- ;
  49:  DO[75:RBT FUORI ING]=ON ;
  50:   ;
  51:  !-- Fine cambio pezzo ----------- ;
  52:  !  e.. ;
  53:  !-- Start ciclo ----------------- ;
  54:  DO[74:START MU]=PULSE,1.0sec ;
  55:   ;
/POS
/END
