/PROG  CBM_CODE
/ATTR
OWNER		= MNEDITOR;
COMMENT		= "CAMBIO  CODE";
PROG_SIZE	= 1328;
CREATE		= DATE 13-04-11  TIME 02:12:00;
MODIFIED	= DATE 13-04-12  TIME 00:07:32;
FILE_NAME	= CICLO_SV;
VERSION		= 0;
LINE_COUNT	= 67;
MEMORY_SIZE	= 1836;
PROTECT		= READ_WRITE;
TCD:  STACK_SIZE	= 0,
      TASK_PRIORITY	= 50,
      TIME_SLICE	= 0,
      BUSY_LAMP_OFF	= 0,
      ABORT_REQUEST	= 0,
      PAUSE_REQUEST	= 0;
DEFAULT_GROUP	= 1,*,*,*,*;
CONTROL_CODE	= 00000000 00000000;
/APPL
/MN
   1:  !------------------------------- ;
   2:  !SETTO  CODE ;
   3:  !------------------------------- ;
   4:   ;
   5:  LBL[10] ;
   6:  IF (F[10:ABILITA RICETTA]),JMP LBL[12] ;
   7:  R[98:NUM_PRIORITA]=0    ;
   8:  R[99:NUM PZ.]=0    ;
   9:  R[34:CODE MU]=0    ;
  10:  R[35:NUM PZ]=0    ;
  11:  R[36:CNT_PZ]=0    ;
  12:  CALL MESSAGGI(18) ;
  13:   ;
  14:  PAUSE ;
  15:  CALL MESSAGGI(1) ;
  16:  CALL WEB_PAGE('PAG2') ;
  17:  PAUSE ;
  18:   ;
  19:  JMP LBL[10] ;
  20:  LBL[12] ;
  21:  R[99:NUM PZ.]=R[98:NUM_PRIORITA]    ;
  22:  R[96:APP.PZ]=R[99:NUM PZ.]+200    ;
  23:  R[35:NUM PZ]=R[R[96]]    ;
  24:  R[95:APP.CODE]=R[98:NUM_PRIORITA]+100    ;
  25:  R[34:CODE MU]=R[R[95]]    ;
  26:   ;
  27:  !------------------------------- ;
  28:  !CONTROLLO CODE ;
  29:  !------------------------------- ;
  30:   ;
  31:  IF R[34:CODE MU]<255,JMP LBL[1] ;
  32:   ;
  33:  R[34:CODE MU]=0    ;
  34:  R[35:NUM PZ]=0    ;
  35:  UALM[11] ;
  36:  LBL[1] ;
  37:   ;
  38:  IF R[35:NUM PZ]>R[36:CNT_PZ],JMP LBL[2] ;
  39:  R[98:NUM_PRIORITA]=R[98:NUM_PRIORITA]+1    ;
  40:  R[99:NUM PZ.]=R[99:NUM PZ.]+1    ;
  41:  R[36:CNT_PZ]=0    ;
  42:  R[R[96]]=0    ;
  43:   ;
  44:  LBL[13] ;
  45:  IF R[98:NUM_PRIORITA]<21,JMP LBL[10] ;
  46:   ;
  47:  R[98:NUM_PRIORITA]=0    ;
  48:  R[99:NUM PZ.]=0    ;
  49:   ;
  50:  CALL WEB_PAGE('PAG11') ;
  51:  F[10:ABILITA RICETTA]=(OFF) ;
  52:  LBL[20] ;
  53:  IF R[30:SETTO RIEMP1]=1,JMP LBL[11] ;
  54:  JMP LBL[10] ;
  55:  LBL[2] ;
  56:   ;
  57:  IF R[98:NUM_PRIORITA]>20,JMP LBL[13] ;
  58:   ;
  59:  IF R[35:NUM PZ]>=R[36:CNT_PZ],JMP LBL[11] ;
  60:   ;
  61:  R[R[96]]=0    ;
  62:  R[98:NUM_PRIORITA]=R[98:NUM_PRIORITA]+1    ;
  63:  R[99:NUM PZ.]=R[99:NUM PZ.]+1    ;
  64:  R[36:CNT_PZ]=0    ;
  65:  JMP LBL[10] ;
  66:  LBL[11] ;
  67:   ;
/POS
/END
