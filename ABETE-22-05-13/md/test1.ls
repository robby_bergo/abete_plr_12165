/PROG  TEST1
/ATTR
OWNER		= MNEDITOR;
COMMENT		= "";
PROG_SIZE	= 625;
CREATE		= DATE 13-04-03  TIME 21:54:54;
MODIFIED	= DATE 13-04-06  TIME 02:27:06;
FILE_NAME	= TEST;
VERSION		= 0;
LINE_COUNT	= 5;
MEMORY_SIZE	= 1109;
PROTECT		= READ_WRITE;
TCD:  STACK_SIZE	= 0,
      TASK_PRIORITY	= 50,
      TIME_SLICE	= 0,
      BUSY_LAMP_OFF	= 0,
      ABORT_REQUEST	= 0,
      PAUSE_REQUEST	= 0;
DEFAULT_GROUP	= 1,1,*,*,*;
CONTROL_CODE	= 00000000 00000000;
/APPL

AUTO_SINGULARITY_HEADER;
  ENABLE_SINGULARITY_AVOIDANCE   : TRUE;
/MN
   1:  LBL[1] ;
   2:   ;
   3:J P[1] 100% FINE    ;
   4:J P[2] 100% FINE    ;
   5:  JMP LBL[1] ;
/POS
P[1]{
   GP1:
	UF : 0, UT : 1,		CONFIG : 'N U T, 0, 0, 0',
	X =      .028  mm,	Y =  -383.925  mm,	Z =    31.900  mm,
	W =      .004 deg,	P =      .000 deg,	R =   -90.000 deg
   GP2:
	UF : 0, UT : 10,	
	J1=     0.000  mm,	J2=     0.000  mm
};
P[2]{
   GP1:
	UF : 0, UT : 1,		CONFIG : 'N U T, 0, 0, 0',
	X =      .028  mm,	Y =  -383.925  mm,	Z =    31.900  mm,
	W =      .004 deg,	P =      .000 deg,	R =   -90.000 deg
   GP2:
	UF : 0, UT : 10,	
	J1=   200.001  mm,	J2=  2000.000  mm
};
/END
