/PROG  ARRAY2_A
/ATTR
OWNER		= MNEDITOR;
COMMENT		= "Conteggio pallet";
PROG_SIZE	= 764;
CREATE		= DATE 07-12-08  TIME 21:46:20;
MODIFIED	= DATE 12-10-05  TIME 19:05:20;
FILE_NAME	= ;
VERSION		= 0;
LINE_COUNT	= 37;
MEMORY_SIZE	= 1248;
PROTECT		= READ_WRITE;
TCD:  STACK_SIZE	= 0,
      TASK_PRIORITY	= 50,
      TIME_SLICE	= 0,
      BUSY_LAMP_OFF	= 0,
      ABORT_REQUEST	= 0,
      PAUSE_REQUEST	= 0;
DEFAULT_GROUP	= *,*,*,*,*;
CONTROL_CODE	= 00000000 00000000;
/MN
   1:  !------------------------------- ;
   2:  !Calcola le coordinate 2D ;
   3:  !sul pallet ;
   4:  !Argomenti: ;
   5:  !AR[1] = 1 Azzera ;
   6:  != 2 Calcola ;
   7:  != 3 Incrementa ;
   8:  !------------------------------- ;
   9:  JMP LBL[AR[1]] ;
  10:   ;
  11:   ;
  12:  LBL[1] ;
  13:  R[3:PRESA NULLA]=0    ;
  14:  R[20:Passo riga X]=0    ;
  15:  R[21:Passo Colonna Y]=0    ;
  16:  R[28:PALLET  FINITO]=0    ;
  17:  END ;
  18:   ;
  19:   ;
  20:  LBL[2] ;
  21:  R[22:OFFSET Riga X]=R[10:Passo X]*R[20:Passo riga X]    ;
  22:  R[23:OFFSET Colonna Y]=R[11:Passo Y]*R[21:Passo Colonna Y]    ;
  23:  END ;
  24:   ;
  25:   ;
  26:  LBL[3] ;
  27:  !Incremento contatori ;
  28:  R[20:Passo riga X]=R[20:Passo riga X]+1    ;
  29:  IF R[20:Passo riga X]<R[12:Max PZ X],JMP LBL[30] ;
  30:  R[20:Passo riga X]=0    ;
  31:  R[21:Passo Colonna Y]=R[21:Passo Colonna Y]+1    ;
  32:  IF R[21:Passo Colonna Y]<R[13:Max PZ Y],JMP LBL[30] ;
  33:  R[28:PALLET  FINITO]=1    ;
  34:  LBL[30] ;
  35:  END ;
  36:   ;
  37:   ;
/POS
/END
