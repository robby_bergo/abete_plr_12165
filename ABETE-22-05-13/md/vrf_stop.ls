/PROG  VRF_STOP
/ATTR
OWNER		= MNEDITOR;
COMMENT		= "";
PROG_SIZE	= 833;
CREATE		= DATE 08-01-01  TIME 14:00:00;
MODIFIED	= DATE 13-05-22  TIME 17:38:52;
FILE_NAME	= ;
VERSION		= 0;
LINE_COUNT	= 21;
MEMORY_SIZE	= 1269;
PROTECT		= READ_WRITE;
TCD:  STACK_SIZE	= 0,
      TASK_PRIORITY	= 50,
      TIME_SLICE	= 0,
      BUSY_LAMP_OFF	= 0,
      ABORT_REQUEST	= 0,
      PAUSE_REQUEST	= 0;
DEFAULT_GROUP	= 1,*,*,*,*;
CONTROL_CODE	= 00000000 00000000;
/MN
   1:  !------- ---- ---- ------ ------ ;
   2:  ! Verifica richieste ;
   3:  !------------------------------- ;
   4:   ;
   5:  !Richiedo apertura porte ;
   6:  IF (!F[3:RICH.AP.PORT.OP]),JMP LBL[31] ;
   7:  CALL HOME_RBT    ;
   8:  F[3:RICH.AP.PORT.OP]=(OFF) ;
   9:  DO[11019]=ON ;
  10:  DO[11018]=ON ;
  11:  PAUSE ;
  12:  LBL[31] ;
  13:   ;
  14:  IF (!F[4:VAI PARCHEGGIO]),JMP LBL[20] ;
  15:  UFRAME_NUM=0 ;
  16:  UTOOL_NUM=0 ;
  17:J P[1] 50% FINE    ;
  18:   ;
  19:  LBL[20] ;
  20:  !Parcheggio  momentaneo ;
  21:  IF (F[4:VAI PARCHEGGIO]),CALL PARCHEGG ;
/POS
P[1]{
   GP1:
	UF : 0, UT : 0,	
	J1=   -89.999 deg,	J2=   -33.000 deg,	J3=     -.002 deg,
	J4=     -.001 deg,	J5=   -90.001 deg,	J6=     -.001 deg
};
/END
