/PROG  CAS_ZERO
/ATTR
OWNER		= MNEDITOR;
COMMENT		= "AZZERA CASSETTI";
PROG_SIZE	= 404;
CREATE		= DATE 11-08-31  TIME 16:46:44;
MODIFIED	= DATE 13-05-22  TIME 18:29:34;
FILE_NAME	= CASS_AZZ;
VERSION		= 0;
LINE_COUNT	= 10;
MEMORY_SIZE	= 868;
PROTECT		= READ_WRITE;
TCD:  STACK_SIZE	= 0,
      TASK_PRIORITY	= 50,
      TIME_SLICE	= 0,
      BUSY_LAMP_OFF	= 0,
      ABORT_REQUEST	= 0,
      PAUSE_REQUEST	= 0;
DEFAULT_GROUP	= *,*,*,*,*;
CONTROL_CODE	= 00000000 00000000;
/APPL
/MN
   1:  !------------------------------ ;
   2:  ! Azzero R da 51 a 74 ;
   3:  !------------------------------ ;
   4:  R[41:*]=51    ;
   5:  LBL[1] ;
   6:  R[R[41]]=0    ;
   7:  R[41:*]=R[41:*]+1    ;
   8:  IF R[41:*]<=74,JMP LBL[1] ;
   9:  R[79:CNT CASSETTI]=0    ;
  10:  R[80:ULTIMO CAS]=0    ;
/POS
/END
