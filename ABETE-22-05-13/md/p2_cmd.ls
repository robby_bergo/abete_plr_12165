/PROG  P2_CMD	  Macro
/ATTR
OWNER		= MNEDITOR;
COMMENT		= "APRI-CHIUDI 2";
PROG_SIZE	= 292;
CREATE		= DATE 12-10-04  TIME 23:22:52;
MODIFIED	= DATE 13-04-05  TIME 16:57:12;
FILE_NAME	= P1_CMD;
VERSION		= 0;
LINE_COUNT	= 10;
MEMORY_SIZE	= 756;
PROTECT		= READ_WRITE;
TCD:  STACK_SIZE	= 0,
      TASK_PRIORITY	= 50,
      TIME_SLICE	= 0,
      BUSY_LAMP_OFF	= 0,
      ABORT_REQUEST	= 0,
      PAUSE_REQUEST	= 0;
DEFAULT_GROUP	= *,*,*,*,*;
CONTROL_CODE	= 00000000 00000000;
/APPL
/MN
   1:   ;
   2:  IF (DO[133:CHIUD.PZ.PLT]),JMP LBL[1] ;
   3:  DO[125:APRI PZ.PLT]=OFF ;
   4:  DO[133:CHIUD.PZ.PLT]=ON ;
   5:   ;
   6:  END ;
   7:  LBL[1] ;
   8:  DO[133:CHIUD.PZ.PLT]=OFF ;
   9:  DO[125:APRI PZ.PLT]=ON ;
  10:   ;
/POS
/END
