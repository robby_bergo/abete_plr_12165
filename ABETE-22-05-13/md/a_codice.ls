/PROG  A_CODICE
/ATTR
OWNER		= MNEDITOR;
COMMENT		= "CODICE";
PROG_SIZE	= 2500;
CREATE		= DATE 13-04-12  TIME 16:53:38;
MODIFIED	= DATE 13-05-15  TIME 00:38:56;
FILE_NAME	= CODICE;
VERSION		= 0;
LINE_COUNT	= 82;
MEMORY_SIZE	= 2800;
PROTECT		= READ_WRITE;
TCD:  STACK_SIZE	= 0,
      TASK_PRIORITY	= 50,
      TIME_SLICE	= 0,
      BUSY_LAMP_OFF	= 0,
      ABORT_REQUEST	= 0,
      PAUSE_REQUEST	= 0;
DEFAULT_GROUP	= 1,*,*,*,*;
CONTROL_CODE	= 00000000 00000000;
/APPL
/MN
   1:  !AZZERAMENTO QUOTE PER CICLO AUTO ;
   2:  !DA CAMBIARE X CAMBIO PRODUZIONE ;
   3:  !-------------------------------- ;
   4:  !Prelievo e deposito pezzo ;
   5:  !-------------------------------- ;
   6:  !POSIZ.ASSE X PALLET ;
   7:  R[9:POS_CASSET.LAV]=200    ;
   8:   ;
   9:  !PASSO in X ;
  10:  R[10:Passo X]=103    ;
  11:   ;
  12:  !PASSO in Y ;
  13:  R[11:Passo Y]=295    ;
  14:   ;
  15:  !N�pezzi in X SU PALLET ;
  16:  R[12:Max PZ X]=5    ;
  17:   ;
  18:  !N�pezzi in Y SU PALLET ;
  19:  R[13:Max PZ Y]=2    ;
  20:   ;
  21:  !N� FREQUENZA PEZZI CAMPIONE ;
  22:  R[15:N.FREQ.PZ.CAMP.]=0    ;
  23:   ;
  24:  !ALTEZZA Interas SC.PEZZO CAMPION ;
  25:  R[16:H.INT.PZ.CAMP.]=5    ;
  26:   ;
  27:  !OFFSET IN Z PALLET ;
  28:  R[24:OFFSET IN Z]=30    ;
  29:   ;
  30:  !FINE DATI X CAMBIO PRODUZIONE ;
  31:  !-------------------------------- ;
  32:  PR[15:OFFSET_CAMP]=PR[49:Zero XYZ NUT]    ;
  33:  PR[15,3:OFFSET_CAMP]=70    ;
  34:   ;
  35:  PR[16:CALC_CAMP]=PR[49:Zero XYZ NUT]    ;
  36:  PR[16,3:CALC_CAMP]=R[16:H.INT.PZ.CAMP.]    ;
  37:   ;
  38:   ;
  39:  PR[1:Prelievo Pz]=PR[48:ZERO XYZ FUT]    ;
  40:  PR[1,1:Prelievo Pz]=1    ;
  41:  PR[1,2:Prelievo Pz]=0    ;
  42:  PR[1,3:Prelievo Pz]=0    ;
  43:  PR[1,4:Prelievo Pz]=(-.634)    ;
  44:  PR[1,5:Prelievo Pz]=(-37.881)    ;
  45:  PR[1,6:Prelievo Pz]=90.572    ;
  46:   ;
  47:  PR[2:Deposito Pz]=PR[48:ZERO XYZ FUT]    ;
  48:  PR[2,1:Deposito Pz]=1    ;
  49:  PR[2,2:Deposito Pz]=0    ;
  50:  PR[2,3:Deposito Pz]=0    ;
  51:  PR[2,4:Deposito Pz]=(-.634)    ;
  52:  PR[2,5:Deposito Pz]=(-37.881)    ;
  53:  PR[2,6:Deposito Pz]=90.572    ;
  54:   ;
  55:  PR[3:Campione]=PR[48:ZERO XYZ FUT]    ;
  56:  PR[3,1:Campione]=0    ;
  57:  PR[3,2:Campione]=0    ;
  58:  PR[3,3:Campione]=30    ;
  59:  PR[3,4:Campione]=(-2.56)    ;
  60:  PR[3,5:Campione]=(-39.827)    ;
  61:  PR[3,6:Campione]=2.03    ;
  62:   ;
  63:  PR[5:Scarico CN]=PR[49:Zero XYZ NUT]    ;
  64:  PR[5,1:Scarico CN]=R[26:OFFSET X PR MU]*(-1)    ;
  65:  PR[5,2:Scarico CN]=0    ;
  66:  PR[5,3:Scarico CN]=(-4)    ;
  67:  PR[5,4:Scarico CN]=(-.414)    ;
  68:  PR[5,5:Scarico CN]=(-38.98)    ;
  69:  PR[5,6:Scarico CN]=.152    ;
  70:   ;
  71:  PR[6:Carico CN]=PR[49:Zero XYZ NUT]    ;
  72:  PR[6,1:Carico CN]=0    ;
  73:  PR[6,2:Carico CN]=0    ;
  74:  PR[6,3:Carico CN]=0    ;
  75:  PR[6,4:Carico CN]=(-.414)    ;
  76:  PR[6,5:Carico CN]=(-38.98)    ;
  77:  PR[6,6:Carico CN]=.152    ;
  78:   ;
  79:  R[1:CONTA PEZZI TOT]=0    ;
  80:   ;
  81:  END ;
  82:   ;
/POS
P[6:"Pos.Cari.CNC_2"]{
   GP1:
	UF : 5, UT : 1,		CONFIG : 'N U T, 0, 0, 1',
	X =     0.000  mm,	Y =     0.000  mm,	Z =     1.000  mm,
	W =   -89.998 deg,	P =      .002 deg,	R =    89.993 deg
};
P[7:"Pos.Scar.CNC_2"]{
   GP1:
	UF : 4, UT : 2,		CONFIG : 'N U T, 0, 0, 0',
	X =     0.000  mm,	Y =     0.000  mm,	Z =     0.000  mm,
	W =    90.002 deg,	P =     -.001 deg,	R =   -90.006 deg
};
P[13]{
   GP1:
	UF : 4, UT : 2,		CONFIG : 'N U T, 0, 0, 0',
	X =    50.653  mm,	Y =   943.493  mm,	Z =   674.084  mm,
	W =    90.002 deg,	P =     -.001 deg,	R =    -1.616 deg
};
P[14]{
   GP1:
	UF : 5, UT : 1,	
	J1=  -166.630 deg,	J2=   -39.731 deg,	J3=    -2.687 deg,
	J4=     -.086 deg,	J5=   -87.528 deg,	J6=   118.959 deg
};
P[15]{
   GP1:
	UF : 5, UT : 1,		CONFIG : 'N U T, 0, 0, 0',
	X =  -130.350  mm,	Y =  1531.512  mm,	Z =   693.311  mm,
	W =   -89.906 deg,	P =      .204 deg,	R =  -132.014 deg
};
P[16]{
   GP1:
	UF : 4, UT : 2,		CONFIG : 'N U T, 0, 0, 0',
	X =    84.708  mm,	Y =  1378.194  mm,	Z =   614.003  mm,
	W =    90.175 deg,	P =      .402 deg,	R =  -133.222 deg
};
P[23]{
   GP1:
	UF : 4, UT : 2,		CONFIG : 'N U T, 0, 0, 0',
	X =    40.980  mm,	Y =   165.879  mm,	Z =   248.977  mm,
	W =    90.002 deg,	P =     -.001 deg,	R =   -90.004 deg
};
P[24]{
   GP1:
	UF : 4, UT : 2,		CONFIG : 'N U T, 0, 0, 0',
	X =   -50.002  mm,	Y =   109.399  mm,	Z =   164.020  mm,
	W =    90.002 deg,	P =     -.001 deg,	R =   -90.005 deg
};
P[25]{
   GP1:
	UF : 5, UT : 1,		CONFIG : 'N U T, 0, 0, 1',
	X =    36.401  mm,	Y =   141.849  mm,	Z =   235.442  mm,
	W =   -89.998 deg,	P =      .002 deg,	R =    89.993 deg
};
P[26]{
   GP1:
	UF : 5, UT : 1,		CONFIG : 'N U T, 0, 0, 1',
	X =   -49.999  mm,	Y =   115.208  mm,	Z =   186.482  mm,
	W =   -89.998 deg,	P =      .002 deg,	R =    89.993 deg
};
P[27]{
   GP1:
	UF : 5, UT : 1,		CONFIG : 'N U T, 0, 0, 1',
	X =    36.401  mm,	Y =   141.849  mm,	Z =   235.442  mm,
	W =   -89.998 deg,	P =      .002 deg,	R =    89.993 deg
};
P[30]{
   GP1:
	UF : 5, UT : 1,		CONFIG : 'N U T, 0, 0, 1',
	X =   -49.999  mm,	Y =   115.208  mm,	Z =   186.482  mm,
	W =   -89.998 deg,	P =      .002 deg,	R =    89.993 deg
};
/END
