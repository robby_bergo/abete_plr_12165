/PROG  MORSA_PR_RIEMP
/ATTR
OWNER		= MNEDITOR;
COMMENT		= "PREL_MORSA";
PROG_SIZE	= 1955;
CREATE		= DATE 13-04-10  TIME 19:03:24;
MODIFIED	= DATE 13-04-12  TIME 01:09:46;
FILE_NAME	= MORSA_PR;
VERSION		= 0;
LINE_COUNT	= 75;
MEMORY_SIZE	= 2299;
PROTECT		= READ_WRITE;
TCD:  STACK_SIZE	= 0,
      TASK_PRIORITY	= 50,
      TIME_SLICE	= 0,
      BUSY_LAMP_OFF	= 0,
      ABORT_REQUEST	= 0,
      PAUSE_REQUEST	= 0;
DEFAULT_GROUP	= 1,*,*,*,*;
CONTROL_CODE	= 00000000 00000000;
/APPL

AUTO_SINGULARITY_HEADER;
  ENABLE_SINGULARITY_AVOIDANCE   : FALSE;
/MN
   1:  UFRAME_NUM=0 ;
   2:  UTOOL_NUM=1 ;
   3:   ;
   4:  WAIT DI[137:PRES.PZ.ATTREZ.]=OFF    ;
   5:  !-- Attendo MU AUTO------ ;
   6:  WAIT DI[76:MACC.IN FUNZIONE]=ON AND DI[77:MACC.IN AUTO]=ON    ;
   7:  ! Attendo Porta Aperta ;
   8:  WAIT DI[79:PORTA AP.MU]=ON    ;
   9:  ! OK al movimento ;
  10:  WAIT DI[67:ASSI FUOR.ING]=ON    ;
  11:  ! Attendo Richiesta Scarico/Car ;
  12:  WAIT DI[65:MAC.PRONTA M20]=ON    ;
  13:  WAIT    .50(sec) ;
  14:  !-- RBT Fuori ingombro ---------- ;
  15:  DO[75:RBT FUORI ING]=OFF ;
  16:   ;
  17:J P[1] 50% CNT50    ;
  18:J P[2] 50% CNT50    ;
  19:J P[3] 50% CNT50    ;
  20:  PR[51:*]=PR[49:Zero XYZ NUT]    ;
  21:  PR[51,1:*]=100    ;
  22:  PR[51,2:*]=0    ;
  23:  PR[51,3:*]=400    ;
  24:   ;
  25:  PR[52:*]=PR[49:Zero XYZ NUT]    ;
  26:  PR[52,1:*]=0    ;
  27:  PR[52,2:*]=0    ;
  28:  PR[52,3:*]=100    ;
  29:   ;
  30:  PR[53:*]=PR[49:Zero XYZ NUT]    ;
  31:  PR[53,1:*]=0    ;
  32:  PR[53,2:*]=(-100)    ;
  33:  PR[53,3:*]=(-5)    ;
  34:   ;
  35:   ;
  36:L P[4] 1000mm/sec CNT50 Tool_Offset,PR[51:*]    ;
  37:L P[4] 500mm/sec CNT10 Tool_Offset,PR[52:*]    ;
  38:L P[4] 100mm/sec FINE    ;
  39:   ;
  40:  CALL P1_CHIUD    ;
  41:  DO[181:SBLOCC. MAND]=ON ;
  42:  WAIT   1.00(sec) ;
  43:L P[4] 500mm/sec FINE Offset,PR[53:*]    ;
  44:L P[4] 1000mm/sec CNT50 Tool_Offset,PR[51:*]    ;
  45:J P[3] 50% CNT50    ;
  46:   ;
  47:  PR[51:*]=PR[49:Zero XYZ NUT]    ;
  48:  PR[51,1:*]=100    ;
  49:  PR[51,2:*]=0    ;
  50:  PR[51,3:*]=(-100)    ;
  51:   ;
  52:  PR[52:*]=PR[49:Zero XYZ NUT]    ;
  53:  PR[52,1:*]=0    ;
  54:  PR[52,2:*]=0    ;
  55:  PR[52,3:*]=(-50)    ;
  56:   ;
  57:J P[6] 50% CNT50    ;
  58:   ;
  59:L P[7] 1000mm/sec CNT50 Offset,PR[51:*]    ;
  60:L P[7] 500mm/sec CNT10 Offset,PR[52:*]    ;
  61:L P[7] 100mm/sec FINE    ;
  62:   ;
  63:  WAIT DI[137:PRES.PZ.ATTREZ.]=ON    ;
  64:  DO[181:SBLOCC. MAND]=OFF ;
  65:  WAIT   1.00(sec) ;
  66:  CALL P1_APRI    ;
  67:  WAIT    .50(sec) ;
  68:   ;
  69:L P[7] 500mm/sec CNT10 Offset,PR[52:*]    ;
  70:   ;
  71:L P[7] 1000mm/sec CNT50 Offset,PR[51:*]    ;
  72:J P[3] 50% CNT50    ;
  73:J P[2] 50% CNT50    ;
  74:J P[1] 50% CNT50    ;
  75:   ;
/POS
P[1]{
   GP1:
	UF : 0, UT : 1,		CONFIG : 'N U T, 0, 0, 0',
	X =      .027  mm,	Y =  -383.922  mm,	Z =    31.904  mm,
	W =      .004 deg,	P =      .001 deg,	R =   -90.003 deg
};
P[2]{
   GP1:
	UF : 0, UT : 1,		CONFIG : 'N U T, 0, 0, 0',
	X =    -8.087  mm,	Y =  -303.235  mm,	Z =   237.398  mm,
	W =    -1.311 deg,	P =   -33.079 deg,	R =   -90.960 deg
};
P[3]{
   GP1:
	UF : 0, UT : 1,		CONFIG : 'N U T, 0, 0, 0',
	X =     2.146  mm,	Y =   285.726  mm,	Z =   226.632  mm,
	W =    -1.386 deg,	P =   -29.882 deg,	R =    90.237 deg
};
P[4]{
   GP1:
	UF : 0, UT : 1,		CONFIG : 'N U T, 0, 0, 0',
	X =   -43.831  mm,	Y =   961.943  mm,	Z =  -285.577  mm,
	W =    -1.016 deg,	P =   -47.466 deg,	R =    90.708 deg
};
P[6]{
   GP1:
	UF : 0, UT : 1,		CONFIG : 'N U T, 0, 0, 0',
	X =  -412.386  mm,	Y =    26.908  mm,	Z =   607.597  mm,
	W =   179.582 deg,	P =   -25.140 deg,	R =     -.276 deg
};
P[7]{
   GP1:
	UF : 0, UT : 1,		CONFIG : 'N U T, 0, 0, 0',
	X =  -523.331  mm,	Y =    26.907  mm,	Z =   709.517  mm,
	W =   179.802 deg,	P =   -53.482 deg,	R =     -.420 deg
};
/END
