/PROG  CICLO_RIEMPI
/ATTR
OWNER		= MNEDITOR;
COMMENT		= "CICLO_RIEMPI";
PROG_SIZE	= 1241;
CREATE		= DATE 08-01-26  TIME 00:07:06;
MODIFIED	= DATE 13-05-22  TIME 18:47:52;
FILE_NAME	= ;
VERSION		= 0;
LINE_COUNT	= 39;
MEMORY_SIZE	= 1757;
PROTECT		= READ_WRITE;
TCD:  STACK_SIZE	= 0,
      TASK_PRIORITY	= 50,
      TIME_SLICE	= 0,
      BUSY_LAMP_OFF	= 0,
      ABORT_REQUEST	= 0,
      PAUSE_REQUEST	= 0;
DEFAULT_GROUP	= 1,*,*,*,*;
CONTROL_CODE	= 00000000 00000000;
/MN
   1:  !------------------------------- ;
   2:  !Ciclo Riempi ;
   3:  IF R[28:PALLET  FINITO]=1,JMP LBL[1] ;
   4:  CALL CBM_CODE    ;
   5:   ;
   6:  IF R[28:PALLET  FINITO]=1,JMP LBL[1] ;
   7:   ;
   8:  !-- MORSA IN DEPOSITO------------ ;
   9:  UFRAME_NUM=0 ;
  10:  UTOOL_NUM=1 ;
  11:J P[1] 50% CNT50    ;
  12:  !-------------------------------- ;
  13:  !Prelievo Pezzo ;
  14:  CALL ARRAY2_A(2) ;
  15:  CALL PL_PRELI    ;
  16:  !------------------------------ ;
  17:  IF R[28:PALLET  FINITO]=1,JMP LBL[1] ;
  18:   ;
  19:  !Ciclo Carico MU ;
  20:  CALL MU_CARICO    ;
  21:   ;
  22:  !------------------------------- ;
  23:  ! FINE CARCO MU ;
  24:  DO[75:RBT FUORI ING]=ON ;
  25:  GO[10:CODE MU]=R[34:CODE MU] ;
  26:   ;
  27:  DO[73:STROBE CODE MU]=PULSE,1.0sec ;
  28:  WAIT DI[66:CONF.LETT.CODE MU]=ON    ;
  29:  WAIT   1.00(sec) ;
  30:  DO[74:START MU]=PULSE,1.0sec ;
  31:  WAIT DI[80:PORTA CH.MU]=ON    ;
  32:  !------------------------------- ;
  33:  !Richiesta pause ;
  34:  CALL VRF_STOP    ;
  35:   ;
  36:  R[30:SETTO RIEMP1]=1    ;
  37:  LBL[1] ;
  38:  !------------------------------- ;
  39:   ;
/POS
P[1]{
   GP1:
	UF : 0, UT : 1,		CONFIG : 'N U T, 0, 0, 0',
	X =      .027  mm,	Y =  -383.922  mm,	Z =    31.904  mm,
	W =      .004 deg,	P =      .001 deg,	R =   -90.003 deg
};
/END
