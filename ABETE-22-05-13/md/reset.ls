/PROG  RESET
/ATTR
OWNER		= MNEDITOR;
COMMENT		= "Reset RBT";
PROG_SIZE	= 780;
CREATE		= DATE 12-03-15  TIME 23:01:50;
MODIFIED	= DATE 13-04-12  TIME 19:05:08;
FILE_NAME	= ;
VERSION		= 0;
LINE_COUNT	= 34;
MEMORY_SIZE	= 1308;
PROTECT		= READ_WRITE;
TCD:  STACK_SIZE	= 0,
      TASK_PRIORITY	= 50,
      TIME_SLICE	= 0,
      BUSY_LAMP_OFF	= 0,
      ABORT_REQUEST	= 0,
      PAUSE_REQUEST	= 0;
DEFAULT_GROUP	= *,*,*,*,*;
CONTROL_CODE	= 00000000 00000000;
/MN
   1:  !--------------------------- 0B ;
   2:  ! ;
   3:  !------------------------------- ;
   4:  R[1:CONTA PEZZI TOT]=0    ;
   5:  F[1:SBLOCCARE LA CSS]=(ON) ;
   6:  F[2:PARCH. FINE CSS]=(OFF) ;
   7:  F[3:RICH.AP.PORT.OP]=(OFF) ;
   8:  F[8:DESABLE RBT]=(OFF) ;
   9:  R[46:Fine Cassettiera]=0    ;
  10:  DO[12606]=OFF ;
  11:  DO[12601]=OFF ;
  12:   ;
  13:  CALL FKEY_CLR    ;
  14:  MESSAGE[ ] ;
  15:  MESSAGE[ ] ;
  16:  MESSAGE[ATTENZIONE VUOI] ;
  17:  MESSAGE[PARTIRE CON NUOVA] ;
  18:  MESSAGE[CASSETTIERA ?] ;
  19:  MESSAGE[ ] ;
  20:  MESSAGE[CONFERMARE.....] ;
  21:  MESSAGE[ ] ;
  22:  MESSAGE[ ] ;
  23:  CALL FKEY_COM('   SI  ','       ','       ','       ','   NO  ') ;
  24:  CALL TPFKEY('Attesa Risposta:',700) ;
  25:  CALL FKEY_CLR    ;
  26:  CALL MESSAGGI(1) ;
  27:   ;
  28:  IF R[700:DATI]>1,JMP LBL[2] ;
  29:  R[46:Fine Cassettiera]=1    ;
  30:   ;
  31:  LBL[2] ;
  32:  R[700:DATI]=0    ;
  33:   ;
  34:   ;
/POS
/END
