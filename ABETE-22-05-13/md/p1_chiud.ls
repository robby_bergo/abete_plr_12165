/PROG  P1_CHIUD
/ATTR
OWNER		= MNEDITOR;
COMMENT		= "CHIUDI PINZA 1";
PROG_SIZE	= 180;
CREATE		= DATE 08-01-01  TIME 14:00:00;
MODIFIED	= DATE 13-04-05  TIME 16:58:26;
FILE_NAME	= ;
VERSION		= 0;
LINE_COUNT	= 4;
MEMORY_SIZE	= 536;
PROTECT		= READ_WRITE;
TCD:  STACK_SIZE	= 0,
      TASK_PRIORITY	= 50,
      TIME_SLICE	= 0,
      BUSY_LAMP_OFF	= 0,
      ABORT_REQUEST	= 0,
      PAUSE_REQUEST	= 0;
DEFAULT_GROUP	= *,*,*,*,*;
CONTROL_CODE	= 00000000 00000000;
/MN
   1:   ;
   2:  RO[3:APRI PZ-1]=OFF ;
   3:  RO[4:CHIUDI PZ-1]=ON ;
   4:  WAIT    .50(sec) ;
/POS
/END
