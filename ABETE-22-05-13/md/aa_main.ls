/PROG  AA_MAIN
/ATTR
OWNER		= MNEDITOR;
COMMENT		= "PROGRAM.PRINCIP";
PROG_SIZE	= 1560;
CREATE		= DATE 11-02-09  TIME 20:19:30;
MODIFIED	= DATE 13-05-22  TIME 18:54:08;
FILE_NAME	= ;
VERSION		= 0;
LINE_COUNT	= 64;
MEMORY_SIZE	= 2168;
PROTECT		= READ_WRITE;
TCD:  STACK_SIZE	= 0,
      TASK_PRIORITY	= 50,
      TIME_SLICE	= 0,
      BUSY_LAMP_OFF	= 0,
      ABORT_REQUEST	= 0,
      PAUSE_REQUEST	= 0;
DEFAULT_GROUP	= *,*,*,*,*;
CONTROL_CODE	= 00000000 00000000;
/MN
   1:  !------------------------- 11G ;
   2:  !PROGRAMMA PRINCIPALE ;
   3:  !------------------------------ ;
   4:  !Inizializazione ;
   5:  CALL AINIZIO    ;
   6:  CALL RESET    ;
   7:   ;
   8:  IF R[46:Fine Cassettiera]=0,JMP LBL[1] ;
   9:  !------------------------------- ;
  10:  !FINE CASSETTIERA ;
  11:  LBL[3:  fine cassetti] ;
  12:  IF R[46:Fine Cassettiera]=1,CALL FINE_LAV ;
  13:   ;
  14:  !Reset memo cassetti ;
  15:  CALL CAS_ZERO    ;
  16:  CALL HOME_RBT    ;
  17:   ;
  18:  !------------------------------- ;
  19:  !Inizio Ciclo di lavoro ;
  20:  !------------------------------- ;
  21:  !Ricerca cassetto ;
  22:  CALL CASS_RIC    ;
  23:  !------------------------------- ;
  24:  !Conteggio cassetti ;
  25:  LBL[1] ;
  26:  IF DI[143:Pres.cass.a bordo]=ON,JMP LBL[2] ;
  27:  CALL CASS_CAL    ;
  28:  IF R[46:Fine Cassettiera]=1,JMP LBL[3] ;
  29:   ;
  30:  !------------------------------- ;
  31:  !Estrazione cassetto ;
  32:  CALL CASS_PR    ;
  33:   ;
  34:  LBL[2] ;
  35:  !Richiesta pause ;
  36:  CALL VRF_STOP    ;
  37:   ;
  38:  !------------------------------- ;
  39:  !Ciclo RBT ;
  40:  IF (F[8:DESABLE RBT]=OFF),CALL CICLO_RBT ;
  41:   ;
  42:  !------------------------------- ;
  43:  !Rientro cassetto ;
  44:  CALL CASS_DE    ;
  45:   ;
  46:  !Richiesta pause ;
  47:  CALL VRF_STOP    ;
  48:   ;
  49:  LBL[11] ;
  50:  !------------------------------- ;
  51:  IF (F[10:ABILITA RICETTA] OR R[80:ULTIMO CAS]=1 AND R[28:PALLET  FINITO]=1),JMP LBL[10] ;
  52:  CALL MESSAGGI(18) ;
  53:  WAIT   2.00(sec) ;
  54:  CALL MESSAGGI(1) ;
  55:  CALL WEB_PAGE('PAG11') ;
  56:  PAUSE ;
  57:  JMP LBL[11] ;
  58:   ;
  59:  LBL[10] ;
  60:   ;
  61:  !Richiesta pause ;
  62:  CALL VRF_STOP    ;
  63:   ;
  64:  JMP LBL[1] ;
/POS
/END
