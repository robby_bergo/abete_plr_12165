/PROG  MU_PULIZIA
/ATTR
OWNER		= MNEDITOR;
COMMENT		= "MU.IN PULIZIA";
PROG_SIZE	= 968;
CREATE		= DATE 13-05-14  TIME 22:16:36;
MODIFIED	= DATE 13-05-22  TIME 17:47:12;
FILE_NAME	= MU_CARIC;
VERSION		= 0;
LINE_COUNT	= 18;
MEMORY_SIZE	= 1268;
PROTECT		= READ_WRITE;
TCD:  STACK_SIZE	= 0,
      TASK_PRIORITY	= 50,
      TIME_SLICE	= 0,
      BUSY_LAMP_OFF	= 0,
      ABORT_REQUEST	= 0,
      PAUSE_REQUEST	= 0;
DEFAULT_GROUP	= 1,*,*,*,*;
CONTROL_CODE	= 00000000 00000000;
/APPL
/MN
   1:  !--------------------------- 11G ;
   2:  !Soffio morsa ;
   3:  UFRAME_NUM=2 ;
   4:  UTOOL_NUM=1 ;
   5:   ;
   6:J P[7] 100% CNT100    ;
   7:J P[5] 100% CNT100    ;
   8:  RO[7:SOFFIO PUL]=ON ;
   9:L P[1] 1000mm/sec FINE    ;
  10:L P[2] 100mm/sec FINE    ;
  11:L P[3] 100mm/sec FINE    ;
  12:L P[4] 100mm/sec FINE    ;
  13:L P[1] 100mm/sec FINE    ;
  14:   ;
  15:  RO[7:SOFFIO PUL]=OFF ;
  16:J P[5] 100% CNT100    ;
  17:J P[7] 100% CNT100    ;
  18:   ;
/POS
P[1]{
   GP1:
	UF : 2, UT : 1,		CONFIG : 'F U T, 0, 0, -1',
	X =    32.459  mm,	Y =   -19.588  mm,	Z =   157.562  mm,
	W =   -12.084 deg,	P =    84.048 deg,	R =   168.787 deg
};
P[2]{
   GP1:
	UF : 2, UT : 1,		CONFIG : 'F U T, 0, 0, -1',
	X =   177.625  mm,	Y =   -23.620  mm,	Z =   133.366  mm,
	W =   -12.084 deg,	P =    84.048 deg,	R =   168.786 deg
};
P[3]{
   GP1:
	UF : 2, UT : 1,		CONFIG : 'F U T, 0, 0, 0',
	X =   182.597  mm,	Y =    16.700  mm,	Z =   132.538  mm,
	W =   -12.084 deg,	P =    84.048 deg,	R =   168.786 deg
};
P[4]{
   GP1:
	UF : 2, UT : 1,		CONFIG : 'F U T, 0, 0, 0',
	X =    28.765  mm,	Y =    17.419  mm,	Z =   158.177  mm,
	W =   -12.086 deg,	P =    84.049 deg,	R =   168.784 deg
};
P[5]{
   GP1:
	UF : 2, UT : 1,		CONFIG : 'F U T, 0, 0, -1',
	X =   -16.768  mm,	Y =   -19.589  mm,	Z =   205.563  mm,
	W =   -12.085 deg,	P =    84.048 deg,	R =   168.786 deg
};
P[7]{
   GP1:
	UF : 2, UT : 1,		CONFIG : 'N U T, 0, 0, 0',
	X =  -100.000  mm,	Y =      .000  mm,	Z =   100.000  mm,
	W =     -.414 deg,	P =   -38.980 deg,	R =      .152 deg
};
/END
