/PROG  COM_PLC1
/ATTR
OWNER		= MNEDITOR;
COMMENT		= "";
PROG_SIZE	= 1203;
CREATE		= DATE 13-05-14  TIME 22:41:58;
MODIFIED	= DATE 13-05-14  TIME 22:41:58;
FILE_NAME	= COM_PLC;
VERSION		= 0;
LINE_COUNT	= 49;
MEMORY_SIZE	= 1539;
PROTECT		= READ_WRITE;
TCD:  STACK_SIZE	= 0,
      TASK_PRIORITY	= 50,
      TIME_SLICE	= 0,
      BUSY_LAMP_OFF	= 0,
      ABORT_REQUEST	= 0,
      PAUSE_REQUEST	= 0;
DEFAULT_GROUP	= *,*,*,*,*;
CONTROL_CODE	= 00000000 00000000;
/APPL

AUTO_SINGULARITY_HEADER;
  ENABLE_SINGULARITY_AVOIDANCE   : TRUE;
/MN
   1:   ;
   2:  IF (DO[11010]),F[3:RICH.AP.PORT.OP]=(ON) ;
   3:   ;
   4:  IF (!SO[1:Cycle start]),F[3:RICH.AP.PORT.OP]=(OFF) ;
   5:   ;
   6:  IF (!SO[1:Cycle start]),F[4:VAI PARCHEGGIO]=(OFF) ;
   7:   ;
   8:  IF (!SO[1:Cycle start]),F[6:Rich. Campione]=(OFF) ;
   9:   ;
  10:  IF ($MOR_GRP[2].$IN_POSITION[2]=1),DO[11514]=(OFF) ;
  11:   ;
  12:  IF ($MOR_GRP[2].$IN_POSITION[2]=0),DO[11514]=(ON) ;
  13:   ;
  14:  !------------------------------- ;
  15:   ;
  16:  IF (!DI[76:MACC.IN FUNZIONE] AND !DI[77:MACC.IN AUTO] AND SO[7:TP enabled] AND !F[21:CHIUDI MORSA]),F[20:APRI MORSA]=(ON) ;
  17:   ;
  18:  IF (!DI[76:MACC.IN FUNZIONE] AND !DI[77:MACC.IN AUTO] AND SO[7:TP enabled] AND F[21:CHIUDI MORSA]),F[20:APRI MORSA]=(OFF) ;
  19:   ;
  20:  !------------------------------- ;
  21:   ;
  22:  IF (!DI[76:MACC.IN FUNZIONE] AND !DI[77:MACC.IN AUTO] AND SO[7:TP enabled] AND F[20:APRI MORSA]),F[21:CHIUDI MORSA]=(OFF) ;
  23:   ;
  24:  !------------------------------- ;
  25:  IF (SI[8:CE/CR Select b0] OR SI[9:CE/CR Select b1] OR !SO[7:TP enabled]),F[20:APRI MORSA]=(OFF) ;
  26:   ;
  27:  IF (SI[8:CE/CR Select b0] OR SI[9:CE/CR Select b1] OR !SO[7:TP enabled]),F[21:CHIUDI MORSA]=(OFF) ;
  28:  !------------------------------- ;
  29:   ;
  30:  IF (DO[191:BLOCCA MORSA] AND SO[7:TP enabled]),DO[183:SBLOC MORSA]=(OFF) ;
  31:   ;
  32:  IF (DI[79:PORTA AP.MU] AND !DI[77:MACC.IN AUTO] AND SO[7:TP enabled] AND F[20:APRI MORSA]),DO[183:SBLOC MORSA]=(ON) ;
  33:   ;
  34:   ;
  35:  !------------------------------- ;
  36:   ;
  37:  IF (DO[183:SBLOC MORSA] AND SO[7:TP enabled]),DO[191:BLOCCA MORSA]=(OFF) ;
  38:   ;
  39:  IF (!DI[76:MACC.IN FUNZIONE] AND !DI[77:MACC.IN AUTO] AND SO[7:TP enabled] AND F[21:CHIUDI MORSA]),DO[191:BLOCCA MORSA]=(ON) ;
  40:   ;
  41:   ;
  42:   ;
  43:  !------------------------------- ;
  44:   ;
  45:   ;
  46:   ;
  47:   ;
  48:   ;
  49:   ;
/POS
/END
