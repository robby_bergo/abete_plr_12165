/PROG  CASS_PR
/ATTR
OWNER		= MNEDITOR;
COMMENT		= "Prelievo cassett";
PROG_SIZE	= 1870;
CREATE		= DATE 06-11-22  TIME 19:01:00;
MODIFIED	= DATE 13-05-22  TIME 18:35:56;
FILE_NAME	= ;
VERSION		= 0;
LINE_COUNT	= 76;
MEMORY_SIZE	= 2366;
PROTECT		= READ_WRITE;
TCD:  STACK_SIZE	= 0,
      TASK_PRIORITY	= 50,
      TIME_SLICE	= 0,
      BUSY_LAMP_OFF	= 0,
      ABORT_REQUEST	= 0,
      PAUSE_REQUEST	= 0;
DEFAULT_GROUP	= *,1,*,*,*;
CONTROL_CODE	= 00000000 00000000;
/MN
   1:  CALL HOME_RBT    ;
   2:  WAIT DO[11501]=ON    ;
   3:   ;
   4:  UFRAME_NUM=0 ;
   5:  UTOOL_NUM=10 ;
   6:   ;
   7:  IF DI[143:Pres.cass.a bordo]=OFF,JMP LBL[10] ;
   8:  CALL MESSAGGI(3) ;
   9:  PAUSE ;
  10:  CALL MESSAGGI(1) ;
  11:  JMP LBL[11] ;
  12:  LBL[10] ;
  13:   ;
  14:  PR[50:*]=LPOS    ;
  15:   ;
  16:  PR[51:*]=PR[49:Zero XYZ NUT]    ;
  17:  PR[51,1:*]=PR[50,1:*]    ;
  18:  PR[51,2:*]=33+R[43:Op.Quota presa]    ;
  19:   ;
  20:  ! In Quota prelievo ;
  21:  PR[51,1:*]=550    ;
  22:L PR[51:*] 1000mm/sec FINE    ;
  23:   ;
  24:  ! Pinza plt Ch ;
  25:  CALL P2_CHIUD    ;
  26:  WAIT DI[143:Pres.cass.a bordo]=OFF    ;
  27:   ;
  28:  ! Posizione inizio spinta CS ;
  29:  PR[51,1:*]=657    ;
  30:L PR[51:*] 1000mm/sec CNT100    ;
  31:  ! Spingo il cassetto ;
  32:  ! Ritorno x svincolo ;
  33:  PR[51,1:*]=655    ;
  34:L PR[51:*] 800mm/sec FINE    ;
  35:   ;
  36:  CALL P2_APRI    ;
  37:   ;
  38:  ! Quota di presa ;
  39:  PR[51,1:*]=705    ;
  40:L PR[51:*] 400mm/sec FINE ACC100    ;
  41:  ! Aggancio CS ;
  42:  CALL P2_CHIUD    ;
  43:   ;
  44:  ! 1'Quota per ripresa ;
  45:  PR[51,1:*]=252    ;
  46:L PR[51:*] 1000mm/sec FINE    ;
  47:  ! Blocco il CS ;
  48:  CALL P2_APRI    ;
  49:   ;
  50:  LBL[12] ;
  51:  IF DI[144:BLOC.CAS.INSER]=ON,JMP LBL[11] ;
  52:  LBL[13] ;
  53:  CALL MESSAGGI(5) ;
  54:  PAUSE ;
  55:  CALL MESSAGGI(1) ;
  56:  JMP LBL[12] ;
  57:  LBL[11] ;
  58:  WAIT (DI[144:BLOC.CAS.INSER])    ;
  59:   ;
  60:  ! 2'Quota per ripresa ;
  61:  PR[51,1:*]=582    ;
  62:L PR[51:*] 1000mm/sec FINE    ;
  63:  ! Aggancio CS ;
  64:  CALL P2_CHIUD    ;
  65:  ! Sblocco il CS ;
  66:  WAIT (!DI[144:BLOC.CAS.INSER]) TIMEOUT,LBL[13] ;
  67:   ;
  68:  ! Quota di lavoro RBT ;
  69:  PR[51,1:*]=R[9:POS_CASSET.LAV]    ;
  70:L PR[51:*] 1000mm/sec FINE    ;
  71:   ;
  72:  WAIT DI[143:Pres.cass.a bordo]=ON    ;
  73:  !AZZERAMENTO PALLET ;
  74:  CALL ARRAY2_A(1) ;
  75:   ;
  76:  R[50:Numero Cassetti]=R[50:Numero Cassetti]+1    ;
/POS
P[1]{
   GP2:
	UF : 0, UT : 10,	
	J1=   679.000  mm,	J2=    22.000  mm
};
P[2]{
   GP2:
	UF : 0, UT : 10,	
	J1=   677.000  mm,	J2=    22.000  mm
};
P[3]{
   GP2:
	UF : 0, UT : 10,	
	J1=   705.000  mm,	J2=    22.000  mm
};
P[4]{
   GP2:
	UF : 0, UT : 10,	
	J1=      .500  mm,	J2=    22.000  mm
};
P[5]{
   GP2:
	UF : 0, UT : 10,	
	J1=   330.000  mm,	J2=    22.000  mm
};
P[6]{
   GP2:
	UF : 0, UT : 10,	
	J1=   230.000  mm,	J2=    22.000  mm
};
/END
