/PROG  RBT_CICL
/ATTR
OWNER		= MNEDITOR;
COMMENT		= "Ciclo RBT";
PROG_SIZE	= 1149;
CREATE		= DATE 08-01-26  TIME 00:07:06;
MODIFIED	= DATE 12-03-15  TIME 23:53:36;
FILE_NAME	= ;
VERSION		= 0;
LINE_COUNT	= 36;
MEMORY_SIZE	= 1693;
PROTECT		= READ_WRITE;
TCD:  STACK_SIZE	= 0,
      TASK_PRIORITY	= 50,
      TIME_SLICE	= 0,
      BUSY_LAMP_OFF	= 0,
      ABORT_REQUEST	= 0,
      PAUSE_REQUEST	= 0;
DEFAULT_GROUP	= 1,*,*,*,*;
CONTROL_CODE	= 00000000 00000000;
/MN
   1:  !------------------------------- ;
   2:  !Ciclo robot ;
   3:  CALL HOME_RBT    ;
   4:  CALL HOME_CAS    ;
   5:   ;
   6:  CALL ARRAY2_A(1) ;
   7:   ;
   8:  !------------------------------ ;
   9:  LBL[2:INCREMENTO] ;
  10:   ;
  11:  CALL ARRAY2_A(2) ;
  12:  CALL PL_PRELI    ;
  13:   ;
  14:  UFRAME_NUM=0 ;
  15:  UTOOL_NUM=10 ;
  16:J P[1] 60% CNT100    ;
  17:  !------------------------------ ;
  18:   ;
  19:  JMP LBL[200] ;
  20:  CALL MU_DEMO    ;
  21:  !------------------------------ ;
  22:  LBL[200] ;
  23:  CALL PL_DEPOS    ;
  24:  CALL ARRAY2_A(3) ;
  25:   ;
  26:   ;
  27:  !------------------------------ ;
  28:  CALL VRF_STOP    ;
  29:   ;
  30:  IF R[20:Passo riga X]<R[12:Max PZ X],JMP LBL[2] ;
  31:   ;
  32:   ;
  33:  !------------------------------- ;
  34:  !Fine ciclo robot ;
  35:  CALL HOME_RBT    ;
  36:  CALL HOME_CAS    ;
/POS
P[1]{
   GP1:
	UF : 0, UT : 10,		CONFIG : 'N U T, 0, 0, 0',
	X =     -.009  mm,	Y =   348.594  mm,	Z =   275.770  mm,
	W =   179.996 deg,	P =      .003 deg,	R =   -37.998 deg
};
/END
