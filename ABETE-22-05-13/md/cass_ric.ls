/PROG  CASS_RIC
/ATTR
OWNER		= MNEDITOR;
COMMENT		= "ricerca cassetto";
PROG_SIZE	= 1271;
CREATE		= DATE 06-11-04  TIME 11:46:44;
MODIFIED	= DATE 13-05-22  TIME 18:32:38;
FILE_NAME	= ;
VERSION		= 0;
LINE_COUNT	= 50;
MEMORY_SIZE	= 1711;
PROTECT		= READ_WRITE;
TCD:  STACK_SIZE	= 0,
      TASK_PRIORITY	= 50,
      TIME_SLICE	= 0,
      BUSY_LAMP_OFF	= 0,
      ABORT_REQUEST	= 0,
      PAUSE_REQUEST	= 0;
DEFAULT_GROUP	= *,1,*,*,*;
CONTROL_CODE	= 00000000 00000000;
/MN
   1:  !------------------------------ ;
   2:  !  Ricerca Cassetti ;
   3:  !------------------------------ ;
   4:   ;
   5:  LBL[11] ;
   6:  IF DI[143:Pres.cass.a bordo]=OFF,JMP LBL[10] ;
   7:  CALL MESSAGGI(3) ;
   8:  PAUSE ;
   9:  CALL MESSAGGI(1) ;
  10:  JMP LBL[11] ;
  11:  LBL[10] ;
  12:   ;
  13:  UFRAME_NUM=0 ;
  14:  UTOOL_NUM=10 ;
  15:   ;
  16:  WAIT DO[11501]=ON    ;
  17:   ;
  18:  R[50:Numero Cassetti]=1    ;
  19:  PR[53:*]=PR[49:Zero XYZ NUT]    ;
  20:   ;
  21:   ;
  22:  LBL[1] ;
  23:   ;
  24:L P[1] 800mm/sec CNT5 Offset,PR[53:*]    ;
  25:  ! TEST ;
  26:  R[48:AppNumCass.]=R[50:Numero Cassetti]+50    ;
  27:  R[R[48]]=0    ;
  28:  IF DI[127:RICERCA.CASSET]=OFF,JMP LBL[3] ;
  29:  ! Cassetto PRESENTE ---------- ;
  30:  R[48:AppNumCass.]=R[50:Numero Cassetti]+50    ;
  31:  R[R[48]]=1    ;
  32:  R[79:CNT CASSETTI]=R[79:CNT CASSETTI]+1    ;
  33:  LBL[3] ;
  34:  !------------------------------ ;
  35:   ;
  36:  R[49:QUOTA CASS]=R[50:Numero Cassetti]*50    ;
  37:  PR[53,2:*]=R[49:QUOTA CASS]    ;
  38:  ! Incremento ;
  39:  R[50:Numero Cassetti]=R[50:Numero Cassetti]+1    ;
  40:  IF R[50:Numero Cassetti]>24,JMP LBL[2] ;
  41:  JMP LBL[1] ;
  42:   ;
  43:   ;
  44:  LBL[2] ;
  45:  !     fine ricerca ;
  46:  R[50:Numero Cassetti]=0    ;
  47:  R[49:QUOTA CASS]=0    ;
  48:  R[47:Presa cass.OK]=1    ;
  49:  R[46:Fine Cassettiera]=0    ;
  50:   ;
/POS
P[1]{
   GP2:
	UF : 0, UT : 10,	
	J1=   550.000  mm,	J2=   115.000  mm
};
/END
