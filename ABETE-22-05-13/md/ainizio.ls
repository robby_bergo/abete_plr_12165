/PROG  AINIZIO
/ATTR
OWNER		= MNEDITOR;
COMMENT		= "Gestione HOME";
PROG_SIZE	= 1523;
CREATE		= DATE 05-05-09  TIME 21:19:42;
MODIFIED	= DATE 13-04-10  TIME 16:09:58;
FILE_NAME	= ;
VERSION		= 0;
LINE_COUNT	= 55;
MEMORY_SIZE	= 1967;
PROTECT		= READ_WRITE;
TCD:  STACK_SIZE	= 0,
      TASK_PRIORITY	= 50,
      TIME_SLICE	= 0,
      BUSY_LAMP_OFF	= 0,
      ABORT_REQUEST	= 0,
      PAUSE_REQUEST	= 0;
DEFAULT_GROUP	= 1,*,*,*,*;
CONTROL_CODE	= 00000000 00000000;
/MN
   1:  !*** Gestione Home TP *** ;
   2:  LBL[1] ;
   3:   ;
   4:  CALL MESSAGGI(1) ;
   5:  MESSAGE[ ] ;
   6:  MESSAGE[ATTENZIONE ROBOT AUTO!] ;
   7:  MESSAGE[ ] ;
   8:  MESSAGE[Mettere Il Robot] ;
   9:  MESSAGE[In Manuale Per Muoverlo] ;
  10:  MESSAGE[In Una Posizione Sicura] ;
  11:  MESSAGE[Per Poter Raggiungere] ;
  12:  MESSAGE[La Posizione Di HOME!] ;
  13:  MESSAGE[ ] ;
  14:  CALL FKEY_COM('   SI  ','       ','       ','       ','   NO  ') ;
  15:  CALL TPFKEY('Attesa Risposta:',700) ;
  16:  CALL FKEY_CLR    ;
  17:  CALL MESSAGGI(1) ;
  18:   ;
  19:  IF R[700:DATI]<>1,JMP LBL[1] ;
  20:  R[200:DATI]=0    ;
  21:   ;
  22:  CALL FKEY_CLR    ;
  23:  MESSAGE[ ] ;
  24:  MESSAGE[ ] ;
  25:  MESSAGE[ATTENZIONE Il Robot Pu�] ;
  26:  MESSAGE[Raggiungere La] ;
  27:  MESSAGE[Posizione Di HOME?] ;
  28:  MESSAGE[ ] ;
  29:  MESSAGE[ATTENZIONE Sei Sicuro?] ;
  30:  MESSAGE[ ] ;
  31:  MESSAGE[ ] ;
  32:  CALL FKEY_COM('   MUOVI','       ','       ','       ','   NO  ') ;
  33:  CALL TPFKEY('Attesa Risposta:',700) ;
  34:  CALL FKEY_CLR    ;
  35:  CALL MESSAGGI(1) ;
  36:   ;
  37:  IF R[700:DATI]<>1,JMP LBL[1] ;
  38:  R[700:DATI]=0    ;
  39:   ;
  40:  UFRAME_NUM=0 ;
  41:  UTOOL_NUM=0 ;
  42:   ;
  43:  IF DO[11501]=OFF,JMP LBL[10] ;
  44:   ;
  45:J P[1] 10% FINE    ;
  46:  JMP LBL[20] ;
  47:   ;
  48:  LBL[10] ;
  49:  JMP LBL[1] ;
  50:   ;
  51:  LBL[20] ;
  52:   ;
  53:  END ;
  54:   ;
  55:   ;
/POS
P[1]{
   GP1:
	UF : 0, UT : 0,		CONFIG : 'N U T, 0, 0, 0',
	X =     -.000  mm,	Y =  -383.928  mm,	Z =   301.914  mm,
	W =  -180.000 deg,	P =      .001 deg,	R =   -90.000 deg
};
/END
