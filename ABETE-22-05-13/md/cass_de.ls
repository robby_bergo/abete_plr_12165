/PROG  CASS_DE
/ATTR
OWNER		= MNEDITOR;
COMMENT		= "Deposito cassett";
PROG_SIZE	= 1474;
CREATE		= DATE 07-01-25  TIME 08:25:18;
MODIFIED	= DATE 13-04-05  TIME 19:48:46;
FILE_NAME	= ;
VERSION		= 0;
LINE_COUNT	= 60;
MEMORY_SIZE	= 1898;
PROTECT		= READ_WRITE;
TCD:  STACK_SIZE	= 0,
      TASK_PRIORITY	= 50,
      TIME_SLICE	= 0,
      BUSY_LAMP_OFF	= 0,
      ABORT_REQUEST	= 0,
      PAUSE_REQUEST	= 0;
DEFAULT_GROUP	= *,1,*,*,*;
CONTROL_CODE	= 00000000 00000000;
/MN
   1:  CALL HOME_RBT    ;
   2:   ;
   3:  UFRAME_NUM=0 ;
   4:  UTOOL_NUM=10 ;
   5:  WAIT DO[11501]=ON    ;
   6:   ;
   7:  PR[50:*]=LPOS    ;
   8:   ;
   9:  PR[51:*]=PR[49:Zero XYZ NUT]    ;
  10:  PR[51,1:*]=PR[50,1:*]    ;
  11:  PR[51,2:*]=33+R[43:Op.Quota presa]    ;
  12:   ;
  13:  ! In Quota deposito ;
  14:  PR[51,1:*]=R[9:POS_CASSET.LAV]    ;
  15:L PR[51:*] 1000mm/sec FINE    ;
  16:   ;
  17:  WAIT DI[143:Pres.cass.a bordo]=ON    ;
  18:   ;
  19:  ! 2'Quota per ripresa ;
  20:  PR[51,1:*]=582    ;
  21:L PR[51:*] 1000mm/sec FINE    ;
  22:  ! Aggancio CS ;
  23:  CALL P2_APRI    ;
  24:  ! Blocco il CS ;
  25:   ;
  26:  LBL[12] ;
  27:  IF (DI[144:BLOC.CAS.INSER] AND !DI[143:Pres.cass.a bordo]),JMP LBL[11] ;
  28:  LBL[13] ;
  29:  CALL P2_APRI    ;
  30:  CALL MESSAGGI(5) ;
  31:  PAUSE ;
  32:  CALL MESSAGGI(1) ;
  33:  JMP LBL[12] ;
  34:  LBL[11] ;
  35:  WAIT (DI[144:BLOC.CAS.INSER] AND !DI[143:Pres.cass.a bordo])    ;
  36:   ;
  37:  ! 1'Quota per ripresa ;
  38:  PR[51,1:*]=252    ;
  39:L PR[51:*] 1000mm/sec FINE    ;
  40:   ;
  41:  ! Aggancio CS ;
  42:  CALL P2_CHIUD    ;
  43:  WAIT (!DI[144:BLOC.CAS.INSER]) TIMEOUT,LBL[13] ;
  44:   ;
  45:  ! Quota di rilascio SC ;
  46:  PR[51,1:*]=705    ;
  47:L PR[51:*] 1000mm/sec FINE    ;
  48:   ;
  49:  WAIT DI[143:Pres.cass.a bordo]=OFF    ;
  50:   ;
  51:  ! Aggancio CS ;
  52:  CALL P2_APRI    ;
  53:   ;
  54:  ! Quota di ritorno ;
  55:  PR[51,1:*]=550    ;
  56:L PR[51:*] 1000mm/sec FINE    ;
  57:   ;
  58:  R[47:Presa cass.OK]=R[47:Presa cass.OK]+1    ;
  59:   ;
  60:   ;
/POS
P[3]{
   GP2:
	UF : 0, UT : 10,	
	J1=   702.000  mm,	J2=    22.000  mm
};
P[7]{
   GP2:
	UF : 0, UT : 10,	
	J1=   330.000  mm,	J2=    22.000  mm
};
P[8]{
   GP2:
	UF : 0, UT : 10,	
	J1=     0.000  mm,	J2=    22.000  mm
};
P[9]{
   GP2:
	UF : 0, UT : 10,	
	J1=   550.000  mm,	J2=    22.000  mm
};
/END
