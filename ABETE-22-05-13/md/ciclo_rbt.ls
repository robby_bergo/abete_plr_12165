/PROG  CICLO_RBT
/ATTR
OWNER		= MNEDITOR;
COMMENT		= "Ciclo RBT";
PROG_SIZE	= 1906;
CREATE		= DATE 08-01-26  TIME 00:07:06;
MODIFIED	= DATE 13-05-22  TIME 18:53:12;
FILE_NAME	= ;
VERSION		= 0;
LINE_COUNT	= 82;
MEMORY_SIZE	= 2434;
PROTECT		= READ_WRITE;
TCD:  STACK_SIZE	= 0,
      TASK_PRIORITY	= 50,
      TIME_SLICE	= 0,
      BUSY_LAMP_OFF	= 0,
      ABORT_REQUEST	= 0,
      PAUSE_REQUEST	= 0;
DEFAULT_GROUP	= 1,*,*,*,*;
CONTROL_CODE	= 00000000 00000000;
/MN
   1:  !------------------------------- ;
   2:  !Ciclo robot ;
   3:  CALL HOME_RBT    ;
   4:  CALL HOME_CAS    ;
   5:   ;
   6:  LBL[3] ;
   7:  !------------------------------ ;
   8:  !CICLO DI RIEMPIMENTO ;
   9:  IF (R[30:SETTO RIEMP1]=0),CALL CICLO_RIEMPI ;
  10:   ;
  11:  IF (R[30:SETTO RIEMP1]=1 AND !F[10:ABILITA RICETTA]),CALL CICLO_SVUOTA ;
  12:   ;
  13:  IF R[30:SETTO RIEMP1]=0 AND R[28:PALLET  FINITO]=0,JMP LBL[3] ;
  14:  !------------------------------ ;
  15:  LBL[2:INCREMENTO] ;
  16:  !------------------------------ ;
  17:   ;
  18:  IF R[28:PALLET  FINITO]=1,JMP LBL[1] ;
  19:   ;
  20:  IF (!F[10:ABILITA RICETTA]),JMP LBL[3] ;
  21:   ;
  22:  CALL CBM_CODE    ;
  23:  IF (!F[10:ABILITA RICETTA]),JMP LBL[3] ;
  24:   ;
  25:  !------------------------------- ;
  26:  !Ciclo Scarico MU ;
  27:  CALL MU_SCARICO    ;
  28:  !-------------------------------- ;
  29:  !Richiesta Pezzo Campione ;
  30:   ;
  31:  IF (!F[6:Rich. Campione]),JMP LBL[11] ;
  32:  CALL CAMPIONE    ;
  33:  CALL HOME_RBT    ;
  34:  DO[11019]=ON ;
  35:  DO[11018]=ON ;
  36:  PAUSE ;
  37:  JMP LBL[4] ;
  38:  LBL[11] ;
  39:   ;
  40:  !------------------------------ ;
  41:  !Deposito Pezzo ;
  42:  CALL ARRAY2_A(2) ;
  43:  CALL PL_DEPOS    ;
  44:  LBL[4] ;
  45:  IF R[28:PALLET  FINITO]=1,JMP LBL[10] ;
  46:   ;
  47:  !------------------------------ ;
  48:  !Prelievo Pezzo ;
  49:  CALL ARRAY2_A(2) ;
  50:  CALL PL_PRELI    ;
  51:   ;
  52:  IF R[28:PALLET  FINITO]=1,JMP LBL[10] ;
  53:  !------------------------------ ;
  54:  !Ciclo Carico MU ;
  55:  CALL MU_CARICO    ;
  56:  !------------------------------- ;
  57:  ! FINE CARCO MU ;
  58:  DO[75:RBT FUORI ING]=ON ;
  59:  GO[10:CODE MU]=R[34:CODE MU] ;
  60:   ;
  61:  DO[73:STROBE CODE MU]=PULSE,1.0sec ;
  62:  WAIT DI[66:CONF.LETT.CODE MU]=ON    ;
  63:  WAIT   1.00(sec) ;
  64:  DO[74:START MU]=PULSE,1.0sec ;
  65:  WAIT DI[80:PORTA CH.MU]=ON    ;
  66:   ;
  67:  !------------------------------- ;
  68:  !Richiesta pause ;
  69:  CALL VRF_STOP    ;
  70:   ;
  71:  IF R[28:PALLET  FINITO]=0,JMP LBL[3] ;
  72:   ;
  73:  LBL[1] ;
  74:  !------------------------------ ;
  75:  !CICLO DI SVUOTAMENTO ;
  76:  LBL[10] ;
  77:  IF (R[30:SETTO RIEMP1]=1),CALL CICLO_SVUOTA ;
  78:   ;
  79:  !------------------------------- ;
  80:  !Fine ciclo robot ;
  81:  CALL HOME_RBT    ;
  82:  CALL HOME_CAS    ;
/POS
/END
