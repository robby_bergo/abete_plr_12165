/PROG  TEST_MU
/ATTR
OWNER		= MNEDITOR;
COMMENT		= "";
PROG_SIZE	= 1245;
CREATE		= DATE 13-04-09  TIME 00:05:38;
MODIFIED	= DATE 13-04-09  TIME 03:18:14;
FILE_NAME	= TEST_22;
VERSION		= 0;
LINE_COUNT	= 24;
MEMORY_SIZE	= 1521;
PROTECT		= READ_WRITE;
TCD:  STACK_SIZE	= 0,
      TASK_PRIORITY	= 50,
      TIME_SLICE	= 0,
      BUSY_LAMP_OFF	= 0,
      ABORT_REQUEST	= 0,
      PAUSE_REQUEST	= 0;
DEFAULT_GROUP	= 1,*,*,*,*;
CONTROL_CODE	= 00000000 00000000;
/APPL

AUTO_SINGULARITY_HEADER;
  ENABLE_SINGULARITY_AVOIDANCE   : FALSE;
/MN
   1:  UFRAME_NUM=0 ;
   2:  UTOOL_NUM=1 ;
   3:   ;
   4:   ;
   5:J P[1] 30% FINE    ;
   6:   ;
   7:J P[2] 20% FINE    ;
   8:J P[3] 20% FINE    ;
   9:   ;
  10:J P[7] 20% FINE    ;
  11:   ;
  12:L P[4] 100mm/sec FINE    ;
  13:   ;
  14:L P[6] 100mm/sec FINE    ;
  15:   ;
  16:L P[8] 100mm/sec FINE    ;
  17:L P[5] 100mm/sec FINE    ;
  18:   ;
  19:L P[9] 100mm/sec FINE    ;
  20:   ;
  21:L P[10] 100mm/sec FINE    ;
  22:   ;
  23:L P[12] 100mm/sec FINE    ;
  24:L P[11] 100mm/sec FINE    ;
/POS
P[1]{
   GP1:
	UF : 0, UT : 1,		CONFIG : 'N U T, 0, 0, 0',
	X =      .048  mm,	Y =  -383.912  mm,	Z =    31.898  mm,
	W =      .007 deg,	P =      .001 deg,	R =   -90.001 deg
};
P[2]{
   GP1:
	UF : 0, UT : 1,		CONFIG : 'N U B, 0, 0, 0',
	X =    -7.916  mm,	Y =  -159.411  mm,	Z =   246.701  mm,
	W =     4.110 deg,	P =   -37.282 deg,	R =   -97.914 deg
};
P[3]{
   GP1:
	UF : 0, UT : 1,		CONFIG : 'N U B, 0, 0, 0',
	X =   -20.742  mm,	Y =   207.503  mm,	Z =   295.491  mm,
	W =     6.210 deg,	P =   -47.008 deg,	R =    88.346 deg
};
P[4]{
   GP1:
	UF : 0, UT : 1,		CONFIG : 'N U T, 0, 0, 0',
	X =   -76.528  mm,	Y =   537.326  mm,	Z =     6.571  mm,
	W =     2.471 deg,	P =   -42.865 deg,	R =    87.278 deg
};
P[5]{
   GP1:
	UF : 0, UT : 1,		CONFIG : 'N U T, 0, 0, 0',
	X =   -45.934  mm,	Y =  1153.839  mm,	Z =   -93.478  mm,
	W =     -.384 deg,	P =   -40.091 deg,	R =    90.033 deg
};
P[6]{
   GP1:
	UF : 0, UT : 1,		CONFIG : 'N U T, 0, 0, 0',
	X =   -45.288  mm,	Y =   791.453  mm,	Z =   -79.687  mm,
	W =     1.047 deg,	P =   -41.226 deg,	R =    89.390 deg
};
P[7]{
   GP1:
	UF : 0, UT : 1,		CONFIG : 'N U T, 0, 0, 0',
	X =   -62.925  mm,	Y =   520.642  mm,	Z =   127.821  mm,
	W =     2.257 deg,	P =   -39.344 deg,	R =    87.400 deg
};
P[8]{
   GP1:
	UF : 0, UT : 1,		CONFIG : 'N U T, 0, 0, 0',
	X =   -45.288  mm,	Y =  1017.965  mm,	Z =   -79.687  mm,
	W =     1.047 deg,	P =   -41.226 deg,	R =    89.390 deg
};
P[9]{
   GP1:
	UF : 0, UT : 1,		CONFIG : 'N U T, 0, 0, 0',
	X =   -45.769  mm,	Y =  1169.394  mm,	Z =  -114.227  mm,
	W =     -.384 deg,	P =   -40.091 deg,	R =    90.033 deg
};
P[10]{
   GP1:
	UF : 0, UT : 1,		CONFIG : 'N U T, 0, 0, 0',
	X =   -45.765  mm,	Y =  1162.771  mm,	Z =  -119.776  mm,
	W =     -.383 deg,	P =   -39.960 deg,	R =    90.032 deg
};
P[11]{
   GP1:
	UF : 0, UT : 1,		CONFIG : 'N U T, 0, 0, 0',
	X =   -45.951  mm,	Y =  1159.114  mm,	Z =  -113.095  mm,
	W =     -.478 deg,	P =   -38.763 deg,	R =    90.188 deg
};
P[12]{
   GP1:
	UF : 0, UT : 1,		CONFIG : 'N U T, 0, 0, 0',
	X =   -46.167  mm,	Y =   992.507  mm,	Z =  -113.094  mm,
	W =     -.373 deg,	P =   -38.764 deg,	R =    90.020 deg
};
/END
