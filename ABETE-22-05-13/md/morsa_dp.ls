/PROG  MORSA_DP
/ATTR
OWNER		= MNEDITOR;
COMMENT		= "DEPO_MORSA";
PROG_SIZE	= 2333;
CREATE		= DATE 13-04-09  TIME 19:40:36;
MODIFIED	= DATE 13-04-12  TIME 19:21:18;
FILE_NAME	= MORSA_PR;
VERSION		= 0;
LINE_COUNT	= 97;
MEMORY_SIZE	= 2857;
PROTECT		= READ_WRITE;
TCD:  STACK_SIZE	= 0,
      TASK_PRIORITY	= 50,
      TIME_SLICE	= 0,
      BUSY_LAMP_OFF	= 0,
      ABORT_REQUEST	= 0,
      PAUSE_REQUEST	= 0;
DEFAULT_GROUP	= 1,*,*,*,*;
CONTROL_CODE	= 00000000 00000000;
/APPL

AUTO_SINGULARITY_HEADER;
  ENABLE_SINGULARITY_AVOIDANCE   : FALSE;
/MN
   1:  UFRAME_NUM=0 ;
   2:  UTOOL_NUM=1 ;
   3:   ;
   4:   ;
   5:  LBL[2] ;
   6:  IF DI[76:MACC.IN FUNZIONE]=ON,JMP LBL[5] ;
   7:  CALL MESSAGGI(14) ;
   8:  PAUSE ;
   9:  CALL MESSAGGI(1) ;
  10:  JMP LBL[2] ;
  11:  LBL[5] ;
  12:   ;
  13:  WAIT DI[137:PRES.PZ.ATTREZ.]=ON    ;
  14:  !-- Attendo MU AUTO------ ;
  15:  WAIT DI[76:MACC.IN FUNZIONE]=ON AND DI[77:MACC.IN AUTO]=ON    ;
  16:  ! Attendo Porta Aperta ;
  17:  WAIT DI[79:PORTA AP.MU]=ON    ;
  18:  ! OK al movimento ;
  19:  WAIT DI[67:ASSI FUOR.ING]=ON    ;
  20:  ! Attendo Richiesta Scarico/Car ;
  21:  WAIT DI[65:MAC.PRONTA M20]=ON    ;
  22:  WAIT    .50(sec) ;
  23:  !-- RBT Fuori ingombro ---------- ;
  24:  DO[75:RBT FUORI ING]=OFF ;
  25:   ;
  26:   ;
  27:J P[1] 50% CNT50    ;
  28:J P[2] 50% CNT50    ;
  29:J P[3] 50% CNT50    ;
  30:   ;
  31:  PR[51:*]=PR[49:Zero XYZ NUT]    ;
  32:  PR[51,1:*]=100    ;
  33:  PR[51,2:*]=0    ;
  34:  PR[51,3:*]=(-100)    ;
  35:   ;
  36:  PR[52:*]=PR[49:Zero XYZ NUT]    ;
  37:  PR[52,1:*]=0    ;
  38:  PR[52,2:*]=0    ;
  39:  PR[52,3:*]=(-50)    ;
  40:   ;
  41:  PR[53:*]=PR[49:Zero XYZ NUT]    ;
  42:  PR[53,1:*]=0    ;
  43:  PR[53,2:*]=0    ;
  44:  PR[53,3:*]=100    ;
  45:   ;
  46:J P[5] 50% CNT50    ;
  47:   ;
  48:L P[7] 800mm/sec CNT10 Tool_Offset,PR[53:*]    ;
  49:L P[7] 100mm/sec FINE    ;
  50:   ;
  51:  WAIT DI[137:PRES.PZ.ATTREZ.]=ON    ;
  52:  CALL P1_CHIUD    ;
  53:  DO[12606]=ON ;
  54:  WAIT    .50(sec) ;
  55:  DO[181:SBLOCC. MAND]=ON ;
  56:  WAIT   1.00(sec) ;
  57:   ;
  58:L P[7] 500mm/sec FINE Offset,PR[52:*]    ;
  59:   ;
  60:L P[7] 1000mm/sec CNT50 Offset,PR[51:*]    ;
  61:J P[6] 50% CNT50    ;
  62:J P[8] 50% CNT50    ;
  63:   ;
  64:  PR[51:*]=PR[49:Zero XYZ NUT]    ;
  65:  PR[51,1:*]=100    ;
  66:  PR[51,2:*]=0    ;
  67:  PR[51,3:*]=400    ;
  68:   ;
  69:  PR[52:*]=PR[49:Zero XYZ NUT]    ;
  70:  PR[52,1:*]=0    ;
  71:  PR[52,2:*]=0    ;
  72:  PR[52,3:*]=100    ;
  73:   ;
  74:  PR[53:*]=PR[49:Zero XYZ NUT]    ;
  75:  PR[53,1:*]=0    ;
  76:  PR[53,2:*]=(-100)    ;
  77:  PR[53,3:*]=(-5)    ;
  78:   ;
  79:   ;
  80:L P[4] 1000mm/sec CNT50 Tool_Offset,PR[51:*]    ;
  81:L P[4] 500mm/sec CNT10 Offset,PR[53:*]    ;
  82:  DO[184:SOFFIO PERNI]=PULSE,5.0sec ;
  83:L P[4] 100mm/sec FINE    ;
  84:   ;
  85:  DO[181:SBLOCC. MAND]=OFF ;
  86:  WAIT   1.00(sec) ;
  87:  CALL P1_APRI    ;
  88:  WAIT    .50(sec) ;
  89:   ;
  90:L P[4] 500mm/sec CNT10 Tool_Offset,PR[52:*]    ;
  91:L P[4] 1000mm/sec CNT50 Tool_Offset,PR[51:*]    ;
  92:J P[3] 50% CNT50    ;
  93:  JMP LBL[1] ;
  94:J P[2] 50% CNT50    ;
  95:J P[1] 50% CNT50    ;
  96:  LBL[1] ;
  97:   ;
/POS
P[1]{
   GP1:
	UF : 0, UT : 1,		CONFIG : 'N U T, 0, 0, 0',
	X =      .027  mm,	Y =  -383.922  mm,	Z =    31.904  mm,
	W =      .004 deg,	P =      .001 deg,	R =   -90.003 deg
};
P[2]{
   GP1:
	UF : 0, UT : 1,		CONFIG : 'N U T, 0, 0, 0',
	X =    -8.087  mm,	Y =  -303.235  mm,	Z =   237.398  mm,
	W =    -1.311 deg,	P =   -33.079 deg,	R =   -90.960 deg
};
P[3]{
   GP1:
	UF : 0, UT : 1,		CONFIG : 'N U T, 0, 0, 0',
	X =     2.146  mm,	Y =   285.726  mm,	Z =   226.632  mm,
	W =    -1.386 deg,	P =   -29.882 deg,	R =    90.237 deg
};
P[4]{
   GP1:
	UF : 0, UT : 1,		CONFIG : 'N U T, 0, 0, 0',
	X =   -43.831  mm,	Y =   961.943  mm,	Z =  -285.577  mm,
	W =    -1.016 deg,	P =   -47.466 deg,	R =    90.708 deg
};
P[5]{
   GP1:
	UF : 0, UT : 1,		CONFIG : 'N U T, 0, 0, 0',
	X =  -412.386  mm,	Y =    26.908  mm,	Z =   607.597  mm,
	W =   179.582 deg,	P =   -25.140 deg,	R =     -.276 deg
};
P[6]{
   GP1:
	UF : 0, UT : 1,		CONFIG : 'N U T, 0, 0, 0',
	X =  -328.915  mm,	Y =   239.569  mm,	Z =   368.117  mm,
	W =   -16.464 deg,	P =   -67.098 deg,	R =  -172.770 deg
};
P[7]{
   GP1:
	UF : 0, UT : 1,		CONFIG : 'N U T, 0, 0, 0',
	X =  -523.331  mm,	Y =    26.907  mm,	Z =   709.517  mm,
	W =   179.802 deg,	P =   -53.482 deg,	R =     -.420 deg
};
P[8]{
   GP1:
	UF : 0, UT : 1,		CONFIG : 'N U T, 0, 0, 0',
	X =   -48.117  mm,	Y =   556.402  mm,	Z =   218.607  mm,
	W =     2.066 deg,	P =   -55.166 deg,	R =    88.422 deg
};
/END
