/PROG  TEST_22
/ATTR
OWNER		= MNEDITOR;
COMMENT		= "";
PROG_SIZE	= 804;
CREATE		= DATE 13-04-05  TIME 20:10:20;
MODIFIED	= DATE 13-04-09  TIME 00:04:58;
FILE_NAME	= ;
VERSION		= 0;
LINE_COUNT	= 16;
MEMORY_SIZE	= 1112;
PROTECT		= READ_WRITE;
TCD:  STACK_SIZE	= 0,
      TASK_PRIORITY	= 50,
      TIME_SLICE	= 0,
      BUSY_LAMP_OFF	= 0,
      ABORT_REQUEST	= 0,
      PAUSE_REQUEST	= 0;
DEFAULT_GROUP	= 1,*,*,*,*;
CONTROL_CODE	= 00000000 00000000;
/APPL

AUTO_SINGULARITY_HEADER;
  ENABLE_SINGULARITY_AVOIDANCE   : FALSE;
/MN
   1:  UFRAME_NUM=0 ;
   2:  UTOOL_NUM=1 ;
   3:   ;
   4:   ;
   5:J P[1] 100% FINE    ;
   6:   ;
   7:J P[2] 100% FINE    ;
   8:J P[3] 100% FINE    ;
   9:   ;
  10:L P[4] 100mm/sec FINE    ;
  11:   ;
  12:   ;
  13:L P[5] 100mm/sec FINE    ;
  14:   ;
  15:   ;
  16:   ;
/POS
P[1]{
   GP1:
	UF : 0, UT : 1,		CONFIG : 'N U T, 0, 0, 0',
	X =      .048  mm,	Y =  -383.912  mm,	Z =    31.898  mm,
	W =      .007 deg,	P =      .001 deg,	R =   -90.001 deg
};
P[2]{
   GP1:
	UF : 0, UT : 1,		CONFIG : 'N U T, 0, 0, 0',
	X =    79.962  mm,	Y =  -350.725  mm,	Z =   193.327  mm,
	W =     -.075 deg,	P =   -31.138 deg,	R =   -77.010 deg
};
P[3]{
   GP1:
	UF : 0, UT : 1,		CONFIG : 'N U T, 0, 0, 0',
	X =  -190.265  mm,	Y =    98.771  mm,	Z =   142.885  mm,
	W =     -.014 deg,	P =    -7.138 deg,	R =   152.692 deg
};
P[4]{
   GP1:
	UF : 0, UT : 1,		CONFIG : 'N U T, 0, 0, 0',
	X =  -190.265  mm,	Y =    98.771  mm,	Z =   142.885  mm,
	W =     -.014 deg,	P =    -7.138 deg,	R =   152.692 deg
};
P[5]{
   GP1:
	UF : 0, UT : 1,		CONFIG : 'N U T, 0, 0, 0',
	X =  -193.763  mm,	Y =   978.503  mm,	Z =   -77.502  mm,
	W =     -.070 deg,	P =   -37.064 deg,	R =    91.822 deg
};
/END
