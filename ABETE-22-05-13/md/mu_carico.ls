/PROG  MU_CARICO
/ATTR
OWNER		= MNEDITOR;
COMMENT		= "MU.IN CARICO";
PROG_SIZE	= 1896;
CREATE		= DATE 12-10-05  TIME 21:10:52;
MODIFIED	= DATE 13-05-22  TIME 17:39:50;
FILE_NAME	= T1_DEPOS;
VERSION		= 0;
LINE_COUNT	= 73;
MEMORY_SIZE	= 2412;
PROTECT		= READ_WRITE;
TCD:  STACK_SIZE	= 0,
      TASK_PRIORITY	= 50,
      TIME_SLICE	= 0,
      BUSY_LAMP_OFF	= 0,
      ABORT_REQUEST	= 0,
      PAUSE_REQUEST	= 0;
DEFAULT_GROUP	= 1,*,*,*,*;
CONTROL_CODE	= 00000000 00000000;
/APPL
/MN
   1:  !--------------------------- 11G ;
   2:  !Deposito lavorato ;
   3:  UFRAME_NUM=2 ;
   4:  UTOOL_NUM=1 ;
   5:J P[1] 50% CNT50    ;
   6:   ;
   7:   ;
   8:  LBL[2] ;
   9:  IF DI[76:MACC.IN FUNZIONE]=ON,JMP LBL[5] ;
  10:  CALL MESSAGGI(14) ;
  11:  PAUSE ;
  12:  CALL MESSAGGI(1) ;
  13:  JMP LBL[2] ;
  14:  LBL[5] ;
  15:   ;
  16:  IF RI[1:P1 APERTA]=ON,CALL P1_CHIUD ;
  17:   ;
  18:  PR[51:*]=PR[49:Zero XYZ NUT]    ;
  19:  PR[51,1:*]=(-100)    ;
  20:  PR[51,2:*]=0    ;
  21:  PR[51,3:*]=100    ;
  22:   ;
  23:  PR[52:*]=PR[49:Zero XYZ NUT]    ;
  24:  PR[52,1:*]=0    ;
  25:  PR[52,2:*]=0    ;
  26:  PR[52,3:*]=50    ;
  27:   ;
  28:  PR[53:*]=PR[49:Zero XYZ NUT]    ;
  29:  PR[53,1:*]=0    ;
  30:  PR[53,2:*]=0    ;
  31:  PR[53,3:*]=(-5)    ;
  32:   ;
  33:J P[1] 50% CNT50    ;
  34:J P[2] 50% CNT50    ;
  35:J P[3] 50% CNT50    ;
  36:   ;
  37:  !-- Attendo MU AUTO------ ;
  38:  WAIT DI[76:MACC.IN FUNZIONE]=ON AND DI[77:MACC.IN AUTO]=ON    ;
  39:  ! Attendo Porta Aperta ;
  40:  WAIT DI[79:PORTA AP.MU]=ON    ;
  41:  ! OK al movimento ;
  42:  WAIT DI[67:ASSI FUOR.ING]=ON    ;
  43:  ! Attendo Richiesta Scarico/Car ;
  44:  WAIT DI[65:MAC.PRONTA M20]=ON    ;
  45:  WAIT    .50(sec) ;
  46:  !-- RBT Fuori ingombro ---------- ;
  47:  DO[75:RBT FUORI ING]=OFF ;
  48:   ;
  49:J P[4] 50% CNT50    ;
  50:  CALL MORSA_AP    ;
  51:   ;
  52:  CALL MU_PULIZIA    ;
  53:L PR[6:Carico CN] 2000mm/sec CNT40 Offset,PR[51:*]    ;
  54:L PR[6:Carico CN] 200mm/sec FINE Offset,PR[52:*]    ;
  55:  !------------------------------- ;
  56:L PR[6:Carico CN] 100mm/sec FINE    ;
  57:  SOFTFLOAT[1] ;
  58:   ;
  59:L PR[6:Carico CN] 100mm/sec FINE Offset,PR[53:*]    ;
  60:  !------------------------------- ;
  61:  SOFTFLOAT END ;
  62:  CALL MORSA_CH    ;
  63:  WAIT    .50(sec) ;
  64:  CALL P1_APRI    ;
  65:   ;
  66:L PR[6:Carico CN] 500mm/sec CNT10 Offset,PR[52:*]    ;
  67:L PR[6:Carico CN] 1500mm/sec CNT50 Offset,PR[51:*]    ;
  68:   ;
  69:J P[3] 100% CNT50    ;
  70:  R[30:SETTO RIEMP1]=1    ;
  71:J P[2] 100% CNT80    ;
  72:J P[1] 100% CNT80    ;
  73:  LBL[1] ;
/POS
P[1]{
   GP1:
	UF : 0, UT : 1,		CONFIG : 'N U T, 0, 0, 0',
	X =      .027  mm,	Y =  -383.922  mm,	Z =    31.904  mm,
	W =      .004 deg,	P =      .001 deg,	R =   -90.003 deg
};
P[2]{
   GP1:
	UF : 2, UT : 1,	
	J1=   -90.000 deg,	J2=   -46.974 deg,	J3=    36.807 deg,
	J4=     1.518 deg,	J5=  -126.352 deg,	J6=    -1.633 deg
};
P[3]{
   GP1:
	UF : 2, UT : 1,	
	J1=    90.000 deg,	J2=   -46.974 deg,	J3=    36.807 deg,
	J4=     1.518 deg,	J5=  -126.353 deg,	J6=    -1.633 deg
};
P[4]{
   GP1:
	UF : 2, UT : 1,	
	J1=    90.000 deg,	J2=   -46.974 deg,	J3=    36.807 deg,
	J4=     1.518 deg,	J5=   -83.353 deg,	J6=    -1.633 deg
};
/END
