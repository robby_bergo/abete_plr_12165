/PROG  CASS_CAL
/ATTR
OWNER		= MNEDITOR;
COMMENT		= "Calcola quota CS";
PROG_SIZE	= 818;
CREATE		= DATE 06-11-22  TIME 19:00:10;
MODIFIED	= DATE 11-10-10  TIME 17:47:36;
FILE_NAME	= ;
VERSION		= 0;
LINE_COUNT	= 39;
MEMORY_SIZE	= 1294;
PROTECT		= READ_WRITE;
TCD:  STACK_SIZE	= 0,
      TASK_PRIORITY	= 50,
      TIME_SLICE	= 0,
      BUSY_LAMP_OFF	= 0,
      ABORT_REQUEST	= 0,
      PAUSE_REQUEST	= 0;
DEFAULT_GROUP	= *,1,*,*,*;
CONTROL_CODE	= 00000000 00000000;
/MN
   1:  !--------------------------- 0B ;
   2:  !  Movimento su cassetti presenti ;
   3:  !  x prelievo/Deposito ;
   4:  !------------------------------ ;
   5:   ;
   6:  WAIT DO[11501]=ON    ;
   7:   ;
   8:  UFRAME_NUM=0 ;
   9:  UTOOL_NUM=10 ;
  10:   ;
  11:  LBL[1] ;
  12:   ;
  13:  R[44:Op.A presa cass]=R[47:Presa cass.OK]+50    ;
  14:  R[45:Op.B presa cass]=R[R[44]]    ;
  15:  !verifica cassetto ;
  16:  IF R[45:Op.B presa cass]=0,JMP LBL[2] ;
  17:   ;
  18:  R[41:*]=R[47:Presa cass.OK]*.004    ;
  19:  R[43:Op.Quota presa]=R[47:Presa cass.OK]*50    ;
  20:  R[43:Op.Quota presa]=R[43:Op.Quota presa]+R[41:*]    ;
  21:   ;
  22:  !Pos inizio gia' su primo cass ;
  23:  R[43:Op.Quota presa]=R[43:Op.Quota presa]-50    ;
  24:   ;
  25:  END ;
  26:   ;
  27:   ;
  28:  LBL[2] ;
  29:  !      cassetto successivo ;
  30:  R[47:Presa cass.OK]=R[47:Presa cass.OK]+1    ;
  31:   ;
  32:  IF R[47:Presa cass.OK]>=25,JMP LBL[3] ;
  33:  JMP LBL[1] ;
  34:   ;
  35:   ;
  36:  LBL[3] ;
  37:  !      Fine cassetti ;
  38:  R[46:Fine Cassettiera]=1    ;
  39:   ;
/POS
/END
