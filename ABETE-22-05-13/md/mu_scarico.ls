/PROG  MU_SCARICO
/ATTR
OWNER		= MNEDITOR;
COMMENT		= "MU.IN SCARICO";
PROG_SIZE	= 1798;
CREATE		= DATE 12-10-05  TIME 21:12:10;
MODIFIED	= DATE 13-05-15  TIME 00:30:44;
FILE_NAME	= PL_DEPOS;
VERSION		= 0;
LINE_COUNT	= 67;
MEMORY_SIZE	= 2322;
PROTECT		= READ_WRITE;
TCD:  STACK_SIZE	= 0,
      TASK_PRIORITY	= 50,
      TIME_SLICE	= 0,
      BUSY_LAMP_OFF	= 0,
      ABORT_REQUEST	= 0,
      PAUSE_REQUEST	= 0;
DEFAULT_GROUP	= 1,*,*,*,*;
CONTROL_CODE	= 00000000 00000000;
/APPL
/MN
   1:  !--------------------------- 11G ;
   2:  !Deposito lavorato ;
   3:  UFRAME_NUM=2 ;
   4:  UTOOL_NUM=1 ;
   5:   ;
   6:   ;
   7:  LBL[2] ;
   8:  IF DI[76:MACC.IN FUNZIONE]=ON,JMP LBL[5] ;
   9:  CALL MESSAGGI(14) ;
  10:  PAUSE ;
  11:  CALL MESSAGGI(1) ;
  12:  JMP LBL[2] ;
  13:  LBL[5] ;
  14:   ;
  15:   ;
  16:   ;
  17:  IF RI[1:P1 APERTA]=OFF,CALL P1_APRI ;
  18:   ;
  19:  PR[51:*]=PR[49:Zero XYZ NUT]    ;
  20:  PR[51,1:*]=(-100)    ;
  21:  PR[51,2:*]=0    ;
  22:  PR[51,3:*]=100    ;
  23:   ;
  24:  PR[52:*]=PR[49:Zero XYZ NUT]    ;
  25:  PR[52,1:*]=0    ;
  26:  PR[52,2:*]=0    ;
  27:  PR[52,3:*]=50    ;
  28:   ;
  29:J P[1] 50% CNT50    ;
  30:J P[2] 50% CNT50    ;
  31:J P[3] 50% CNT50    ;
  32:  LBL[1] ;
  33:   ;
  34:  !-- Attendo MU AUTO------ ;
  35:  WAIT DI[76:MACC.IN FUNZIONE]=ON AND DI[77:MACC.IN AUTO]=ON    ;
  36:  ! Attendo Porta Aperta ;
  37:  WAIT DI[79:PORTA AP.MU]=ON    ;
  38:  ! OK al movimento ;
  39:  WAIT DI[67:ASSI FUOR.ING]=ON    ;
  40:  ! Attendo Richiesta Scarico/Car ;
  41:  WAIT DI[65:MAC.PRONTA M20]=ON    ;
  42:  WAIT    .50(sec) ;
  43:  !-- RBT Fuori ingombro ---------- ;
  44:  DO[75:RBT FUORI ING]=OFF ;
  45:   ;
  46:J P[4] 50% CNT50    ;
  47:   ;
  48:L PR[5:Scarico CN] 1500mm/sec CNT80 Offset,PR[51:*]    ;
  49:L PR[5:Scarico CN] 200mm/sec CNT10 Offset,PR[52:*]    ;
  50:   ;
  51:  !------------------------------- ;
  52:L PR[5:Scarico CN] 100mm/sec FINE    ;
  53:  !------------------------------- ;
  54:  CALL P1_CHIUD    ;
  55:  WAIT    .50(sec) ;
  56:  CALL MORSA_AP    ;
  57:   ;
  58:L PR[5:Scarico CN] 500mm/sec FINE Offset,PR[52:*]    ;
  59:L PR[5:Scarico CN] 1200mm/sec CNT100 Offset,PR[51:*]    ;
  60:   ;
  61:J P[3] 100% CNT20    ;
  62:J P[2] 50% CNT50    ;
  63:J P[1] 50% CNT50    ;
  64:   ;
  65:  !-- RBT Fuori ingombro ---------- ;
  66:  DO[75:RBT FUORI ING]=OFF ;
  67:  R[30:SETTO RIEMP1]=0    ;
/POS
P[1]{
   GP1:
	UF : 0, UT : 1,		CONFIG : 'N U T, 0, 0, 0',
	X =      .027  mm,	Y =  -383.922  mm,	Z =    31.904  mm,
	W =      .004 deg,	P =      .001 deg,	R =   -90.003 deg
};
P[2]{
   GP1:
	UF : 2, UT : 1,	
	J1=   -90.000 deg,	J2=   -46.974 deg,	J3=    36.807 deg,
	J4=     1.518 deg,	J5=  -126.352 deg,	J6=    -1.633 deg
};
P[3]{
   GP1:
	UF : 2, UT : 1,		CONFIG : 'N U T, 0, 0, 0',
	X =  -895.124  mm,	Y =   -52.236  mm,	Z =   475.987  mm,
	W =    -1.623 deg,	P =     8.954 deg,	R =    -2.564 deg
};
P[4]{
   GP1:
	UF : 2, UT : 1,	
	J1=    90.000 deg,	J2=   -46.974 deg,	J3=    36.807 deg,
	J4=     1.518 deg,	J5=   -83.353 deg,	J6=    -1.633 deg
};
/END
