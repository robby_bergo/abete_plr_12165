/PROG  CICLO_MU
/ATTR
OWNER		= MNEDITOR;
COMMENT		= "";
PROG_SIZE	= 1418;
CREATE		= DATE 12-03-16  TIME 00:46:36;
MODIFIED	= DATE 12-10-05  TIME 23:28:44;
FILE_NAME	= ;
VERSION		= 0;
LINE_COUNT	= 49;
MEMORY_SIZE	= 1878;
PROTECT		= READ_WRITE;
TCD:  STACK_SIZE	= 0,
      TASK_PRIORITY	= 50,
      TIME_SLICE	= 0,
      BUSY_LAMP_OFF	= 0,
      ABORT_REQUEST	= 0,
      PAUSE_REQUEST	= 0;
DEFAULT_GROUP	= 1,*,*,*,*;
CONTROL_CODE	= 00000000 00000000;
/APPL

AUTO_SINGULARITY_HEADER;
  ENABLE_SINGULARITY_AVOIDANCE   : FALSE;
/MN
   1:  !------------------------------- ;
   2:  UTOOL_NUM=0 ;
   3:  UFRAME_NUM=0 ;
   4:   ;
   5:J P[1] 100% FINE    ;
   6:   ;
   7:  LBL[2] ;
   8:  IF DI[49]=ON,JMP LBL[1] ;
   9:  CALL MESSAGGI(14) ;
  10:  PAUSE ;
  11:  CALL MESSAGGI(1) ;
  12:  JMP LBL[2] ;
  13:  LBL[1] ;
  14:   ;
  15:  ! Attendo Porta Aperta ;
  16:  WAIT DI[33]=ON AND DI[49]=ON    ;
  17:  ! Attendo Richiesta Scarico ;
  18:  WAIT DI[34]=ON    ;
  19:  ! RBT Fuori Ingombro ;
  20:  DO[33]=OFF ;
  21:   ;
  22:  !------------------------------- ;
  23:  !Ciclo Scarico MU ;
  24:  CALL MU_SCARICO    ;
  25:   ;
  26:  ! FINE SCARCO MU ;
  27:  DO[35]=ON ;
  28:   ;
  29:  !------------------------------- ;
  30:  ! Attendo Porta Aperta ;
  31:  WAIT DI[33]=ON AND DI[49]=ON    ;
  32:  ! Attendo Richiesta Carico ;
  33:  WAIT DI[36]=ON    ;
  34:   ;
  35:  !------------------------------- ;
  36:  !Ciclo Carico MU ;
  37:  CALL MU_CARICO    ;
  38:   ;
  39:  UTOOL_NUM=0 ;
  40:  UFRAME_NUM=0 ;
  41:J P[1] 100% FINE    ;
  42:   ;
  43:  ! FINE CARCO MU ;
  44:  DO[37]=ON ;
  45:   ;
  46:  !------------------------------- ;
  47:  ! RBT Fuori Ingombro ;
  48:  DO[33]=ON ;
  49:   ;
/POS
P[1]{
   GP1:
	UF : 0, UT : 0,		CONFIG : 'N U T, 0, 0, 0',
	X =     -.000  mm,	Y =   264.878  mm,	Z =   540.322  mm,
	W =   180.000 deg,	P =     -.000 deg,	R =    90.001 deg
};
/END
